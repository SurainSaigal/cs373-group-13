import json

def read_json_list(file_name):
    """Reads a list of strings from a JSON file."""
    with open(file_name, 'r') as file:
        return json.load(file)

def map_all_strings(lst, first_letter):
    """Maps all strings in the list to {<first_letter><index>: original string value}."""
    result = {}
    counters = {}
    for item in lst:
        # Initialize or update the counter for the first letter
        counters[first_letter] = counters.get(first_letter, 0) + 1
        # Create the key using the first letter and its counter
        key = f"{first_letter}{counters[first_letter]}"
        result[key] = item
    return result

def write_json_map(file_name, data):
    """Writes a map (dictionary) to a JSON file."""
    with open(file_name, 'w') as file:
        json.dump(data, file, indent=4)

def process_and_write_json_map(specified_letter, input_file_name, output_file_name):
    input_list = read_json_list(input_file_name)
    indices = map_all_strings(input_list, specified_letter)
    output_map = {specified_letter: indices}
    write_json_map(output_file_name, output_map)
    print(f"The map has been written to {output_file_name} with the letter '{specified_letter}'.")

process_and_write_json_map('C', '../names/centers.json', './temp_data/dict_centers.json')
process_and_write_json_map('B', '../names/disease.json', './temp_data/dict_disease.json')
process_and_write_json_map('A', '../names/treatments.json', './temp_data/dict_treatments.json')