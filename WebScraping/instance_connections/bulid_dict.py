import json

def combine_json_leaf_entries(file_paths, output_file_path):
    combined_entries = {}

    for file_path in file_paths:
        try:
            with open(file_path, 'r') as file:
                data = json.load(file)
                # Iterate through each top-level key and add its nested entries to the combined dictionary
                for parent_key, child_dict in data.items():
                    combined_entries.update(child_dict)  # This assumes child keys are unique across files or that overlaps should be merged
        except Exception as e:
            print(f"An error occurred while processing '{file_path}': {e}")
            return

    # Save the combined entries to the specified output JSON file
    try:
        with open(output_file_path, 'w') as output_file:
            json.dump(combined_entries, output_file, indent=4)
        print(f"Success: The combined leaf entries have been saved to '{output_file_path}'.")
    except Exception as e:
        print(f"An error occurred while saving the combined entries: {e}")

if __name__ == "__main__":
    # Example usage with three file paths and an output file path
    file_paths = ['./temp_data/dict_treatments.json', './temp_data/dict_disease.json', './temp_data/dict_centers.json']
    output_file_path = './temp_data/LOOKUP_VALUES.json'
    combine_json_leaf_entries(file_paths, output_file_path)
