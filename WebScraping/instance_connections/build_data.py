# Import the random module for selecting references
import json
import random
# Function to add references between objects of two different sets
def add_references(from_set, to_set, num_refs):
    for from_obj in from_set:
        # Keep track of how many references have been successfully added
        added_refs = 0
        while added_refs < num_refs:
            ref = random.choice(to_set)  # Randomly choose an element from `to_set`
            # Add reference to the `from_obj` if not already present
            if ref['id'] not in from_obj[ref['id'][0]]:
                from_obj[ref['id'][0]].append(ref['id'])
                added_refs += 1
                # Add back-reference from `ref` to `from_obj`, checking the limit
                # if len(ref[from_obj['id'][0]]) < 2:
                ref[from_obj['id'][0]].append(from_obj['id'])

# Function to ensure at least 2 references from each set
def ensure_minimum_references(set_A, set_B, set_C):
    # Adjust to dynamically determine the correct key based on the sets being referenced
    def fill_references(current_set, other_set1, other_set1_key, other_set2, other_set2_key):
        for obj in current_set:
            for target_set, target_key in [(other_set1, other_set1_key), (other_set2, other_set2_key)]:
                while len(obj[target_key]) < 2:
                    potential_refs = [o for o in target_set if len(o[obj['id'][0]]) < 5]
                    if not potential_refs:  # If no suitable objects are available, break to avoid an infinite loop
                        break
                    ref = random.choice(potential_refs)
                    if ref['id'] not in obj[target_key]:
                        obj[target_key].append(ref['id'])
                        ref[obj['id'][0]].append(obj['id'])

    # Call fill_references with correct keys for other sets
    fill_references(set_A, set_B, 'B', set_C, 'C')
    fill_references(set_B, set_A, 'A', set_C, 'C')
    fill_references(set_C, set_A, 'A', set_B, 'B')

# Function to print sets in a more readable format
def print_sets(sets):
    for set_name, set_data in sets.items():
        print(f"{set_name}:")
        for obj in set_data:
            references = ", ".join([f"{key}: {obj[key]}" for key in obj.keys() if key != 'id'])
            print(f"  {obj['id']} -> {references}")
        print()  # Add a newline for better separation

# Define the sizes of each set for demonstration
size_A, size_B, size_C = 151, 100, 50

# Initialize the sets with unique identifiers for each object
set_A = [{'id': f'A{i}', 'B': [], 'C': []} for i in range(1, size_A + 1)]
set_B = [{'id': f'B{i}', 'A': [], 'C': []} for i in range(1, size_B + 1)]
set_C = [{'id': f'C{i}', 'A': [], 'B': []} for i in range(1, size_C + 1)]


# Add mutual references between sets, now ensuring no more than 2 references for each type
add_references(set_A, set_B, 2)  # Each A references 2 Bs
add_references(set_A, set_C, 2)  # Each A references 2 Cs
add_references(set_B, set_C, 2)  # Each B references 2 Cs

# Ensure all objects have at least 2 references from each of the other sets
ensure_minimum_references(set_A, set_B, set_C)

# Display the sets with their references in a readable format
# print_sets({'Set A': set_A, 'Set B': set_B, 'Set C': set_C})

# Function to save a set to a JSON file
def save_set_to_file(data_set, file_name):
    with open(file_name, 'w') as file:
        json.dump(data_set, file, indent=4)
    # print(f"{file_name} has been saved.")

# Save each set to its own file
save_set_to_file(set_A, 'temp_data/setA.json')
save_set_to_file(set_B, 'temp_data/setB.json')
save_set_to_file(set_C, 'temp_data/setC.json')