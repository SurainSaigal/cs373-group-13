import json

def read_json(file_path):
    """Reads a JSON file and returns its contents as a list."""
    with open(file_path, 'r') as file:
        return json.load(file)

def list_diff(list1, list2):
    """Compares two lists and prints differences."""
    set1 = set(list1)
    set2 = set(list2)

    added = list(set2 - set1)
    removed = list(set1 - set2)

    if added:
        print(f"Added: {added}")
    if removed:
        print(f"Removed: {removed}")

    # Checking for order changes (optional)
    for i, (item1, item2) in enumerate(zip(list1, list2)):
        if item1 != item2:
            print(f"Order changed at index {i}: '{item1}' in file1 is '{item2}' in file2")

def diff_checker(file1_path, file2_path):
    """Compares two JSON files containing lists and prints differences."""
    list1 = read_json(file1_path)
    list2 = read_json(file2_path)
    
    list_diff(list1, list2)
# Example usage
diff_checker("../names/disease.json",      "./out__diseases.json")
diff_checker("../names/treatments.json",   "./out__treatments.json")
diff_checker("../names/centers.json",      "./out__treatment_centers.json")
print("Comparison complete.")