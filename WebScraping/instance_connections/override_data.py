import json

def override_lists(file_path1, file_path2, output_file_path):
    with open(file_path1, 'r') as file1:
        list1 = json.load(file1)
    
    with open(file_path2, 'r') as file2:
        list2 = json.load(file2)
    
    # Override attributes in objects of list2 with values from corresponding objects in list1
    for index, obj1 in enumerate(list1):
        if index < len(list2):
            obj2 = list2[index]
            # Iterate over keys in obj2 to ensure only existing entries are overridden
            for key in obj2.keys():
                if key == 'name' and 'id' in obj1:
                    # Special case for overriding 'name' with 'id' from obj1
                    obj2['name'] = obj1['id']
                elif key in obj1:
                    # Override existing entry with value from obj1
                    obj2[key] = obj1[key]

    # Save the overridden list to the output file
    with open(output_file_path, 'w') as output_file:
        json.dump(list2, output_file, indent=4)
    print(f"Success: List with overridden entries saved to '{output_file_path}'.")

# List of file path tuples
file_paths = [
    ('./temp_data/finalA.json', '../treatments.json'        , '../treatments.json'),
    ('./temp_data/finalB.json', '../diseases.json'          , '../diseases.json'),
    ('./temp_data/finalC.json', '../treatment_centers.json' , '../treatment_centers.json')
]

# Loop through each tuple and perform the override operation
for file_path1, file_path2, output_file_path in file_paths:
    override_lists(file_path1, file_path2, output_file_path)
