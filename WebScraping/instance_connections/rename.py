import json

def read_json_file(file_name):
    """Reads a JSON file and returns its content."""
    with open(file_name, 'r') as file:
        return json.load(file)

def write_json_file(file_name, data):
    """Writes data to a JSON file."""
    with open(file_name, 'w') as file:
        json.dump(data, file, indent=4)

def replace_keys_in_object(obj):
    """Replaces specific keys in a dictionary based on predefined rules."""
    key_replacements = {
        'A': 'connected_treatments',
        'B': 'connected_diseases',
        'C': 'connected_centers'
    }
    return {key_replacements.get(k, k): v for k, v in obj.items()}

def process_json_data(data):
    """Processes a list of objects, replacing keys according to specified rules."""
    return [replace_keys_in_object(obj) for obj in data]

def process_and_write_json(input_file_name, output_file_name):
    original_data = read_json_file(input_file_name)
    modified_data = process_json_data(original_data)
    write_json_file(output_file_name, modified_data)
    print(f"Processed data has been written to {output_file_name}.")

# Define the pairs of input and output filenames
file_pairs = [
    ('./temp_data/setA.json', './temp_data/set_A.json'),
    ('./temp_data/setB.json', './temp_data/set_B.json'),
    ('./temp_data/setC.json', './temp_data/set_C.json')
]

# Process each pair of files
for input_file, output_file in file_pairs:
    process_and_write_json(input_file, output_file)