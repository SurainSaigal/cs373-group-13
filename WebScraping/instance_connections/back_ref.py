import json

def load_json(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)

def verify_references(setA, setB, setC):
    # Convert sets into dictionaries for easy access
    setA_dict = {item['id']: item for item in setA}
    setB_dict = {item['id']: item for item in setB}
    setC_dict = {item['id']: item for item in setC}

    # Create a combined mapping for easier reference checking and minimum references check
    combined = {'A': setA_dict, 'B': setB_dict, 'C': setC_dict}

    # Check for mutual references and at least two references in each list
    for set_name, current_set in combined.items():
        for item in current_set.values():
            for reference_set_name, reference_ids in item.items():
                if reference_set_name in combined:
                    # Check for at least two references
                    if len(reference_ids) < 2:
                        print(f"{item['id']} does not have at least two references in its {reference_set_name} list.")
                        return False
                    # Check for mutual references
                    for ref_id in reference_ids:
                        if ref_id not in combined[reference_set_name] or item['id'] not in combined[reference_set_name][ref_id][set_name]:
                            print(f"Missing mutual reference between {item['id']} and {ref_id}.")
                            return False
    return True

def main():
    setA = load_json('./temp_data/setA.json')
    setB = load_json('./temp_data/setB.json')
    setC = load_json('./temp_data/setC.json')

    if verify_references(setA, setB, setC):
        print("All references are mutual and each object has at least two references in its lists.")
    else:
        print("Verification failed. Check the output for specific issues.")

if __name__ == "__main__":
    main()
