import json

# Load JSON data
def load_json(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)

# Directly load the lookup_values as a dictionary
lookup_dict = load_json('./temp_data/LOOKUP_VALUES.json')

# Updated function to replace values including the 'id' field
def replace_values(set_file, lookup_dict):
    for item in set_file:
        # Replace 'id' field value
        if 'id' in item:
            item['id'] = lookup_dict.get(item['id'], item['id'])

        # Replace 'connected_diseases' values
        if 'connected_diseases' in item:
            item['connected_diseases'] = [lookup_dict.get(disease, disease) for disease in item['connected_diseases']]

        # Replace 'connected_centers' values
        if 'connected_centers' in item:
            item['connected_centers'] = [lookup_dict.get(center, center) for center in item['connected_centers']]

        # Replace 'connected_treatments' values
        if 'connected_treatments' in item:
            item['connected_treatments'] = [lookup_dict.get(treatment, treatment) for treatment in item['connected_treatments']]

    return set_file

# Load the set files
setT = load_json('./temp_data/set_A.json')
setD = load_json('./temp_data/set_B.json')
setC = load_json('./temp_data/set_C.json')

# Replace the values
setT_updated = replace_values(setT, lookup_dict)
setD_updated = replace_values(setD, lookup_dict)
setC_updated = replace_values(setC, lookup_dict)

# Function to save the updated JSON data
def save_json(file_path, data):
    with open(file_path, 'w') as file:
        json.dump(data, file, indent=4)

# Save the updated set files
save_json('./temp_data/finalA.json', setT_updated)
save_json('./temp_data/finalB.json', setD_updated)
save_json('./temp_data/finalC.json', setC_updated)
