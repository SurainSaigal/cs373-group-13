import json

# Function to extract state and city from address string
def extract_state_city(address):
    parts = address.split(",")
    if len(parts) >= 3:  # Check if there are at least three parts (including state code)
        city = parts[-2].strip()
        state = parts[-1].strip().split()[0]  # Get the first word as the state code
        return city, state
    else:
        return "", ""

# Read JSON data from input file
with open('centers.json', 'r') as input_file:
    data = json.load(input_file)

# Process each object and add state and city
processed_data = []
for obj in data:
    address = obj.get("address", "")
    city, state = extract_state_city(address)
    obj["city"] = city
    obj["state"] = state
    processed_data.append(obj)

# Write processed data to output JSON file
with open('output_data.json', 'w') as output_file:
    json.dump(processed_data, output_file, indent=4)