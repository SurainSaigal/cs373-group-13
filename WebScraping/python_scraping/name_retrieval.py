import json

def extract_names_from_files(file_names):
    names = []

    for file_name in file_names:
        with open(file_name, 'r') as file:
            data = json.load(file)
            # Check if the top-level JSON object is a list
            if isinstance(data, list):
                # Process each item in the list
                for item in data:
                    # Assuming each item in the list is a dictionary
                    if 'name' in item:
                        names.append(item['name'])
                    else:
                        names.append("Name not found in item")
            elif isinstance(data, dict):
                # Process a single dictionary object
                names.append(data.get("name", "Name not found"))
            else:
                # Handle unexpected JSON structure
                print(f"Unexpected JSON structure in {file_name}")

    return names

def write_output(file_name, data):
    with open(file_name, 'w') as file:
        json.dump(data, file, indent=4)

input_files = ['file1.json', 'file2.json', 'file3.json']
names_data = extract_names_from_files(input_files)
write_output('output.json', names_data)

print("Data has been written to output.json")
