import json
import random

def generate_random_data():
    data = {
        "attribute_class": random.choice(['A', 'B', 'C']),
        "attribute_type": random.choice(['Type 1', 'Type 2', 'Type 3']),
        "attribute_cost": random.randint(100, 1000),
        "attribute_availability": random.choice(['High', 'Medium', 'Low']),
        "attribute_success_rate": round(random.uniform(0, 100), 2)
    }
    return data

def populate_json(file_name):
    # Assuming the structure contains multiple treatments, not just "treatment_1"
    treatments = {f"treatment_{i+1}": generate_random_data() for i in range(150)}
    
    with open(file_name, 'w') as json_file:
        json.dump(treatments, json_file, indent=4)

# Example usage
file_name = 'input.json'
populate_json(file_name)
print(f"Data has been populated into {file_name}.")