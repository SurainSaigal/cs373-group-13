
import json
# with open('../treatment_centers_new.json', 'r') as f:
#     file = f.read()

# diseases_new = json.loads(file)

with (open('../treatment_centers.json', 'r')) as f:
    file = f.read()

diseases = json.loads(file)


# assert that the lists are the same length and have the exact same keys
print(len(diseases))
# print(len(diseases_new))
# assert len(diseases) == len(diseases_new)

# old_disease_names, new_disease_names = set(), set()
# for disease in diseases:
#     old_disease_names.add(disease['name'])
# for disease in diseases_new:
#     new_disease_names.add(disease['name'])

# print(len(old_disease_names))
# print(len(new_disease_names))

# assert old_disease_names == new_disease_names

# best_diseases = []
# for disease in diseases:
#     count = 0
#     for disease_new in diseases_new:
#         if disease['name'] == disease_new['name']:
#             count += 1
#             # disease['video'] = disease_new['video']
#             disease['image'] = disease_new['image']
#             best_diseases.append(disease)
#     assert count == 1

# # format the json as a readable string
# diseases_json = json.dumps(best_diseases, indent=2)
# with open('../treatment_centers.json', 'w') as f:
#     f.write(diseases_json)
