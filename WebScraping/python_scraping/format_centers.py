import json

def process_item(item):
    # Extract sizeCapacity and convert to integer if possible, else default to 0
    capacity = 0
    if item.get("sizeCapacity", "").strip("+").isdigit():
        capacity = int(item["sizeCapacity"].strip("+"))

    # Construct the new item structure
    new_item = {
        "name": item.get("name", ""),
        "attribute_state": item.get("state", ""),
        "attribute_capacity": capacity,
        "attribute_class": "",  # Placeholder, as logic to determine this value is not specified
        "attribute_branch": item.get("branch", "").replace(" Center", ""),  # Simplified assumption
        "website": item.get("website", ""),
        "city": item.get("city", ""),
        "address": item.get("address", ""),
        "image": "",  # Assuming no direct mapping, so placeholder is used
        "coordinates": item.get("coordinates", {"lat": 0, "lng": 0}),
        "contact": item.get("contact", ""),
        "description": item.get("description", ""),
        "connected_diseases": [],  # Placeholder, as specific values are not given
        "connected_treatments": []  # Placeholder, as specific values are not given
    }

    return new_item

# Read the input JSON file
with open('input.json', 'r') as file:
    data = json.load(file)

# Check if the data is a list and process accordingly, otherwise process a single dictionary
if isinstance(data, list):
    processed_data = [process_item(item) for item in data]
else:
    processed_data = process_item(data)

# Write the processed data to output.json
with open('output.json', 'w') as file:
    json.dump(processed_data, file, indent=4)

print("The output.json file has been created with the modified structure.")
