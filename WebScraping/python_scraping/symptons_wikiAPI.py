import wikipedia
import json

def extract_symptoms_from_text(text, symptom_list):
    detected_symptoms = []
    for symptom in symptom_list:
        if symptom.lower() in text.lower():
            detected_symptoms.append(symptom)
    return detected_symptoms

def get_symptoms_from_wikipedia(disease_data, symptom_list):
    # Set language to English
    wikipedia.set_lang("en")
    
    # Disease name from JSON data
    disease_name = disease_data['name']
    
    # Search for the Wikipedia page related to the disease
    try:
        page = wikipedia.page(disease_name)
    except wikipedia.exceptions.DisambiguationError as e:
        # If there are multiple options, choose the first one
        page = wikipedia.page(e.options[0])
    except wikipedia.exceptions.PageError as e:
        page = wikipedia.page(disease_name, auto_suggest = False)
    
    # Get the full page content
    page_content = page.content
    
    # Extract symptoms from the content using symptoms from disease data
    symptoms = extract_symptoms_from_text(page_content, symptom_list)
    
    return symptoms

# Define the list of symptoms
symptoms_list = [
    "redness of the conjunctivae",
    "seizures",
    "renal failure",
    "Decrease in visual acuity",
    "bowel ischemia",
    "myocardial infarction",
    "atrophy",
    "ocular pain",
    "redness of the sclera",
    "blindness",
    "anorexia",
    "weight loss",
    "cachexia",
    "chills and shivering",
    "convulsions",
    "deformity",
    "discharge",
    "dizziness / Vertigo",
    "fatigue",
    "malaise",
    "asthenia",
    "hypothermia",
    "jaundice",
    "muscle weakness",
    "pyrexia",
    "sweats",
    "swelling",
    "swollen or painful lymph node(s)",
    "weight gain",
    "arrhythmia",
    "bradycardia",
    "chest pain",
    "claudication",
    "palpitations",
    "tachycardia",
    "dry mouth",
    "epistaxis",
    "halitosis",
    "hearing loss",
    "nasal discharge",
    "otalgia",
    "otorrhea",
    "sore throat",
    "toothache",
    "tinnitus",
    "trismus",
    "abdominal pain",
    "bloating",
    "belching",
    "bleeding:",
    "constipation",
    "diarrhea",
    "dysphagia",
    "dyspepsia",
    "fecal incontinence",
    "flatulence",
    "heartburn",
    "nausea",
    "odynophagia",
    "proctalgia fugax",
    "pyrosis",
    "Rectal tenesmus",
    "steatorrhea",
    "vomiting",
    "alopecia",
    "hirsutism",
    "hypertrichosis",
    "abrasion",
    "anasarca",
    "bleeding into the skin",
    "blister",
    "edema",
    "itching",
    "Janeway lesions and Osler's node",
    "laceration",
    "rash",
    "urticaria",
    "abnormal posturing",
    "acalculia",
    "agnosia",
    "alexia",
    "amnesia",
    "anomia",
    "anosognosia",
    "aphasia and apraxia",
    "apraxia",
    "ataxia",
    "cataplexy",
    "confusion",
    "dysarthria",
    "dysdiadochokinesia",
    "dysgraphia",
    "hallucination",
    "headache",
    "hypokinetic movement disorder:",
    "hyperkinetic movement disorder:",
    "insomnia",
    "Lhermitte's sign",
    "loss of consciousness",
    "Syncope",
    "neck stiffness",
    "opisthotonus",
    "paralysis and paresis",
    "paresthesia",
    "prosopagnosia",
    "somnolence",
    "abnormal vaginal bleeding",
    "vaginal bleeding in early pregnancy / miscarriage",
    "vaginal bleeding in late pregnancy",
    "amenorrhea",
    "infertility",
    "painful intercourse",
    "pelvic pain",
    "vaginal discharge",
    "amaurosis fugax and amaurosis",
    "blurred vision",
    "Dalrymple's sign",
    "double vision",
    "exophthalmos",
    "mydriasis/miosis",
    "nystagmus",
    "amusia",
    "anhedonia",
    "anxiety",
    "apathy",
    "confabulation",
    "depression",
    "delusion",
    "euphoria",
    "homicidal ideation",
    "irritability",
    "mania",
    "paranoid ideation",
    "suicidal ideation",
    "apnea and hypopnea",
    "cough",
    "dyspnea",
    "bradypnea and tachypnea",
    "orthopnea and platypnea",
    "trepopnea",
    "hemoptysis",
    "pleuritic chest pain",
    "sputum production",
    "arthralgia",
    "back pain",
    "sciatica",
    "dysuria",
    "hematospermia",
    "hematuria",
    "impotence",
    "polyuria",
    "retrograde ejaculation",
    "strangury",
    "urethral discharge",
    "urinary frequency",
    "urinary incontinence",
    "urinary retention"
]


# Load disease data from JSON file
with open('../diseases.json') as json_file:
    diseases_data = json.load(json_file)

# Iterate over each disease
for disease_data in diseases_data:
    # Fetch symptoms from Wikipedia for the current disease
    symptoms_from_wikipedia = get_symptoms_from_wikipedia(disease_data, symptoms_list)
    
    # Check if symptoms are found
    if symptoms_from_wikipedia:
        # Update symptoms attribute in the disease data
        disease_data['symptoms'] = symptoms_from_wikipedia
        
        print(f"Detected symptoms for {disease_data['name']}: {symptoms_from_wikipedia}")
    else:
        print(f"No symptoms found for {disease_data['name']} on Wikipedia")

# Write updated disease data back to the JSON file
with open('../diseases.json', 'w') as json_file:
    json.dump(diseases_data, json_file, indent=4)
print(f"Wrote to json.")
