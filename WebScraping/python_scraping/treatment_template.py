import json

# Function to generate a treatment
def generate_treatment():
    return {
        "attribute_class": "",
        "attribute_type": "",
        "attribute_cost": 0,
        "attribute_availability": "",
        "attribute_success_rate": 0
    }

# Generate 150 treatments
treatments = {}
for i in range(150):
    treatment_name = f"treatment_{i+1}"
    treatments[treatment_name] = generate_treatment()

# Write treatments to a JSON file
with open('treatments.json', 'w') as json_file:
    json.dump(treatments, json_file, indent=4)