import openai

# Set your OpenAI API key here
openai.api_key = 'YOUR_API_KEY_HERE'

def get_medical_condition_info(condition_name):
    response = openai.Completion.create(
        engine="davinci", 
        prompt=f"Provide a detailed summary of the following medical condition including its Type, Rarity, Year Discovered, Mortality Rate, Age of Onset, and Leading Cause: {condition_name}.",
        temperature=0.5,
        max_tokens=250,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0
    )
    return response.choices[0].text.strip()

# Example usage
condition_name = "Rheumatoid lung disease"
info = get_medical_condition_info(condition_name)
print(info)