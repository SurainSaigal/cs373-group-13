import json

def transform_keys(input_file, output_file):
    # Function to convert keys to lowercase and replace spaces with underscores
    def convert_keys(obj):
        if isinstance(obj, dict):
            new_obj = {}
            for k, v in obj.items():
                new_key = k.lower().replace(' ', '_')
                new_obj[new_key] = convert_keys(v)
            return new_obj
        elif isinstance(obj, list):
            return [convert_keys(item) for item in obj]
        else:
            return obj

    # Read the input JSON file
    with open(input_file, 'r') as f:
        data = json.load(f)

    # Transform the keys
    transformed_data = convert_keys(data)

    # Write the transformed data to the output JSON file
    with open(output_file, 'w') as f:
        json.dump(transformed_data, f, indent=4)

# Example usage
input_file = 'input.json'
output_file = 'output.json'
transform_keys(input_file, output_file)