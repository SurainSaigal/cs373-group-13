import requests
from bs4 import BeautifulSoup
import re
import json
from time import sleep


def get_first_video(name):
    name = name.replace(' ', '+')
    name = name.replace('\'', '%27')

    request = requests.get(
        'https://www.youtube.com/results?search_query=' + name).text

    soup = BeautifulSoup(request, 'lxml')

    script = soup.find_all("script")

    final = ""
    for i in script:
        m = re.search('var ytInitialData = (.+)[,;]{1}', str(i))
        if m:
            m = m.group(1)
            final += m

    json_data = json.loads(final)
    videos = json_data['contents']['twoColumnSearchResultsRenderer']['primaryContents'][
        'sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents']

    for video in videos:
        if 'videoRenderer' in video:
            primary_video = video['videoRenderer']['videoId']
            video_link = "https://www.youtube.com/embed/" + primary_video
            return video_link


# read a json file
# with open('./WebScraping/diseases.json', 'r') as f:
#     diseases_json = f.read()


# convert the json to a dictionary
# diseases = json.loads(diseases_json)
# diseases_new = []
# for disease in diseases:
#     name = disease['name']
#     video_link = None
#     while not video_link:
#         video_link = get_first_video(name)
#         print(name, video_link)
#     disease['video_link'] = video_link
#     diseases_new.append(disease)
# # format the json as a readable string
# diseases_json = json.dumps(diseases_new, indent=2)
# with open('diseases_new.json', 'w') as f:
#     f.write(diseases_json)
with open('../treatments_new.json', 'r') as f:
    diseases_json = f.read()


# # convert the json to a dictionary
diseases = json.loads(diseases_json)
diseases_new = []
for disease in diseases:
    name = disease['name']
    video_link = None
    while not video_link:
        video_link = get_first_video(name)
        print(name, video_link)
    disease['video'] = video_link
    diseases_new.append(disease)

# format the json as a readable string
diseases_json = json.dumps(diseases_new, indent=2)
with open('../treatments_new.json', 'w') as f:
    f.write(diseases_json)
