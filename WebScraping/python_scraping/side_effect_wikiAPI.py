import wikipedia
import json

def extract_side_effects_from_text(text, side_effects_list):
    detected_side_effects = []
    for side_effect in side_effects_list:
        if side_effect.lower() in text.lower():
            detected_side_effects.append(side_effect)
    return detected_side_effects

def get_side_effects_from_wiki(treatment, side_effects_list):
    # Set language to English
    wikipedia.set_lang("en")
    
    # Disease name from JSON data
    side_effect_name = treatment['name']
    
    # Search for the Wikipedia page related to the disease
    try:
        page = wikipedia.page(side_effect_name, auto_suggest = False)
    except wikipedia.exceptions.PageError as e:
        page = wikipedia.page(side_effect_name, auto_suggest = True)
    except wikipedia.exceptions.DisambiguationError as e:
        # If there are multiple options, choose the first one
        page = wikipedia.page(e.options[0])
    
    
    # Get the full page content
    page_content = page.content
    
    # Extract symptoms from the content using symptoms from disease data
    symptoms = extract_side_effects_from_text(page_content, side_effects_list)
    
    return symptoms

side_effects_list = [
    "anorexia",
    "weight loss",
    "cachexia",
    "chills and shivering",
    "convulsions",
    "deformity",
    "discharge",
    "dizziness / Vertigo",
    "fatigue",
    "malaise",
    "asthenia",
    "hypothermia",
    "jaundice",
    "muscle weakness",
    "pyrexia",
    "sweats",
    "swelling",
    "swollen or painful lymph node(s)",
    "weight gain",
    "arrhythmia",
    "bradycardia",
    "chest pain",
    "claudication",
    "palpitations",
    "tachycardia",
    "dry mouth",
    "epistaxis",
    "halitosis",
    "hearing loss",
    "nasal discharge",
    "otalgia",
    "otorrhea",
    "sore throat",
    "toothache",
    "tinnitus",
    "trismus",
    "abdominal pain",
    "bloating",
    "belching",
    "bleeding:",
    "constipation",
    "diarrhea",
    "dysphagia",
    "dyspepsia",
    "fecal incontinence",
    "flatulence",
    "heartburn",
    "nausea",
    "odynophagia",
    "proctalgia fugax",
    "pyrosis",
    "Rectal tenesmus",
    "steatorrhea",
    "vomiting",
    "alopecia",
    "hirsutism",
    "hypertrichosis",
    "abrasion",
    "anasarca",
    "bleeding into the skin",
    "blister",
    "edema",
    "itching",
    "Janeway lesions and Osler's node",
    "laceration",
    "rash",
    "urticaria",
    "abnormal posturing",
    "acalculia",
    "agnosia",
    "alexia",
    "amnesia",
    "anomia",
    "anosognosia",
    "aphasia and apraxia",
    "apraxia",
    "ataxia",
    "cataplexy",
    "confusion",
    "dysarthria",
    "dysdiadochokinesia",
    "dysgraphia",
    "hallucination",
    "headache",
    "hypokinetic movement disorder:",
    "hyperkinetic movement disorder:",
    "insomnia",
    "Lhermitte's sign",
    "loss of consciousness",
    "Syncope",
    "neck stiffness",
    "opisthotonus",
    "paralysis and paresis",
    "paresthesia",
    "prosopagnosia",
    "somnolence",
    "abnormal vaginal bleeding",
    "vaginal bleeding in early pregnancy / miscarriage",
    "vaginal bleeding in late pregnancy",
    "amenorrhea",
    "infertility",
    "painful intercourse",
    "pelvic pain",
    "vaginal discharge",
    "amaurosis fugax and amaurosis",
    "blurred vision",
    "Dalrymple's sign",
    "double vision",
    "exophthalmos",
    "mydriasis/miosis",
    "nystagmus",
    "amusia",
    "anhedonia",
    "anxiety",
    "apathy",
    "confabulation",
    "depression",
    "delusion",
    "euphoria",
    "homicidal ideation",
    "irritability",
    "mania",
    "paranoid ideation",
    "suicidal ideation",
    "apnea and hypopnea",
    "cough",
    "dyspnea",
    "bradypnea and tachypnea",
    "orthopnea and platypnea",
    "trepopnea",
    "hemoptysis",
    "pleuritic chest pain",
    "sputum production",
    "arthralgia",
    "back pain",
    "sciatica",
    "dysuria",
    "hematospermia",
    "hematuria",
    "impotence",
    "polyuria",
    "retrograde ejaculation",
    "strangury",
    "urethral discharge",
    "urinary frequency",
    "urinary incontinence",
    "urinary retention"
]


# Load disease data from JSON file
with open('../treatments.json') as json_file:
    treatment_data = json.load(json_file)

# Iterate over each disease
for treatment in treatment_data:
    # Fetch symptoms from Wikipedia for the current disease
    side_effects_from_wikipedia = get_side_effects_from_wiki(treatment, side_effects_list)
    
    # Check if symptoms are found
    if side_effects_from_wikipedia:
        # Update symptoms attribute in the disease data
        treatment['side_effects'] = side_effects_from_wikipedia
        
        print(f"Detected side effects for {treatment['name']}: {side_effects_from_wikipedia}")
    else:
        print(f"No side effects  found for {treatment['name']} on Wikipedia")
        treatment['side_effects'] = ["No known side effects"]


# Write updated disease data back to the JSON file
with open('../treatments.json', 'w') as json_file:
    json.dump(treatment_data, json_file, indent=4)
print(f"Wrote to json.")
