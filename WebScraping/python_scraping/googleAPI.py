import json
import googlemaps

# Your Google Maps API key
API_KEY = 'YOUR_API_KEY_HERE'
gmaps = googlemaps.Client(key=API_KEY)

def add_coordinates_to_json(json_file_path):
    # Load the JSON file
    with open(json_file_path, 'r') as file:
        data = json.load(file)
    
    # Iterate through each item and add coordinates
    for item in data:
        # Assuming each item has an 'address' field
        address = item.get('address', '')
        if address:
            # Get the location data from Google Maps
            geocode_result = gmaps.geocode(address)
            if geocode_result:
                # Extract latitude and longitude
                latitude = geocode_result[0]['geometry']['location']['lat']
                longitude = geocode_result[0]['geometry']['location']['lng']
                # Add coordinates to the item
                item['coordinates'] = {'lat': latitude, 'lng': longitude}
    
    # Write the modified data back to the file (or a new file)
    with open(json_file_path, 'w') as file:
        json.dump(data, file, indent=4)

# Replace 'your_file_path.json' with the path to your JSON file
add_coordinates_to_json('./treatment_centers.json')