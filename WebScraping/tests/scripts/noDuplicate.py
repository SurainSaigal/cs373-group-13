import json
import sys

def check_duplicates_in_json(file_path):
    try:
        # Load the list of strings from the JSON file
        with open(file_path, 'r') as file:
            strings = json.load(file)
        
        # Find duplicates
        seen = set()
        duplicates = set()
        for string in strings:
            if string in seen:
                duplicates.add(string)
            else:
                seen.add(string)
        
        # Check if the list has duplicates
        if duplicates:
            print(f"Error: There are duplicate strings in the file '{file_path}': {', '.join(duplicates)}")
        else:
            # Including the size of the string list in the success message
            print(f"Success: No duplicates found in '{file_path}'. Size of the string list: {len(strings)}.")

    except Exception as e:
        print(f"An error occurred while processing '{file_path}': {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 noDuplicate.py <file_path.json>")
    else:
        check_duplicates_in_json(sys.argv[1])
