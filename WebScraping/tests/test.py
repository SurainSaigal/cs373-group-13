import json
import sys

def extract_names(input_file_path, output_file_path):
    try:
        # Load the data from the input JSON file
        with open(input_file_path, 'r') as input_file:
            data = json.load(input_file)

        # Extract the 'name' attribute from each object
        names = [item['name'] for item in data]

        # Write the list of names to the output JSON file
        with open(output_file_path, 'w') as output_file:
            json.dump(names, output_file, indent=4)

        print(f"Success: Extracted names have been saved to '{output_file_path}'.")

    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 extract_names.py <input_file_path.json> <output_file_path.json>")
    else:
        extract_names(sys.argv[1], sys.argv[2])
