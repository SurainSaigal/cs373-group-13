import requests
import json
import os

api_url = 'https://api.autoimmunediseasecenter.me'

# api_url = 'http://127.0.0.1:5000'

# Path to the JSON file
diseases_json_file_path = os.path.join(os.path.dirname(__file__), '..', 'diseases.json')

# Open and read the JSON file
with open(diseases_json_file_path, 'r') as file:
    diseases = json.load(file)

# URL to the API
url = api_url+'/disease'

if diseases:
    # Loop through the diseases and upload them to the API
    for disease in diseases:
        response = requests.post(url, json=disease)
        print(response.json())




treatments_json_file_path = os.path.join(os.path.dirname(__file__), '..', 'treatments.json')

# Open and read the JSON file
with open(treatments_json_file_path, 'r') as file:
    treatments = json.load(file)

# URL to the API
url = api_url+'/treatment'

if treatments:
    # Loop through the treatments and upload them to the API
    for treatment in treatments:
        response = requests.post(url, json=treatment)
        print(response.json())


treatment_centers_json_file_path = os.path.join(os.path.dirname(__file__), '..', 'treatment_centers.json')

# Open and read the JSON file
with open(treatment_centers_json_file_path, 'r') as file:
    treatment_centers = json.load(file)

# URL to the API
url = api_url+'/treatment_center'

if treatment_centers:
    # Loop through the treatment centers and upload them to the API
    for treatment_center in treatment_centers:
        response = requests.post(url, json=treatment_center)
        print(response.json())

