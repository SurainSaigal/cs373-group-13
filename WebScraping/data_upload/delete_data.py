import requests
import json
import os

api_url = 'https://api.autoimmunediseasecenter.me'

# api_url = 'http://127.0.0.1:5000'

requests.delete(api_url+'/disease')
requests.delete(api_url+'/treatment')
requests.delete(api_url+'/treatment_center')
print('Data deleted successfully')