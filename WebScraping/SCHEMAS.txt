Every entry in the database must have all of these fields. 
Fields that begin with "attribute_" are ones we will end up 
using for sorting and filtering. This means that if they are 
categorical (filterable), the value of the field must be one of a 
set of valid values (meaning one disease having the attribute_mortality_rate 
as "varies" and another as "variable" will not work). If the attribute is 
numeric (sortable), make sure the value's scale and format
are the same across all instances.


DISEASES:

    SCHEMA:
        {
            “name”: <disease name>,
            “attribute_type”: <type>,
            “attribute_rarity”:<"Varies", "Common", "Uncommon", "Rare", or "Very Rare">,
            “attribute_year_discovered”: <FOUR DIGIT YEAR>,
            “attribute_mortality_rate”: <"Low", "High", or "Varies">,
            “attribute_age_of_onset”: <RANGE OF AGES IN FORMAT "XX-XX">,
            “leading_cause”: <leading cause string>,
            “overview”: <string of an overview of the disease (can ai generate for now)>,
            “symptoms”: <COMMA SEPARATED LIST OF SYMPTOMS>,
            “video”: <YOUTUBE EMBED LINK>,
            "image": <IMAGE LINK>,
            “connected_treatments”: [<connected treatment 1>, <connected treatment 2>, etc.],
            “connected_centers”: [<connected center 1, connected center 2, etc.],
        }

    DISEASE EXAMPLE:
        {
            “name”: “Addison’s Disease”,
            “attribute_type”: “Endocrine”,
            “attribute_rarity”: “Rare”,
            “attribute_year_discovered”: 1855,
            “attribute_mortality_rate”: “Varies”,
            “attribute_age_of_onset”: “30-50",
            “leading_cause”: “Autoimmune destruction of the adrenal cortex”,
            “overview”: "Addison's disease is a blank caused by blank. It is very dangerous.",
            “symptoms”: ["Fever", "Irritability", "Depression"]
            “video”: "https://www.youtube.com/embed/wzq0PI6rGTw",
            "image": <IMAGE LINK>,
            “cause”: "Caused by eating too many burgers",
            “connected_treatments”: [“Ibuprofen”, “Acetaminophen”],
            “connected_centers”: [“Johns Hopkins Hospital”, “NYU Langone Hospitals”]
        }

TREATMENTS:

    SCHEMA:
        {
            "name": <treatment name>,
            "attribute_class": <class>,
            "attribute_type": <type>,
            "attribute_cost": <cost, integer>,
            "attribute_availability": <availability>,
            "attribute_success_rate": 75,
            "image": <image link>,
            "video": <youtube embed link>,
            "overview": <ai generated. about 100-150 words>,
            "side_effects": ["side effect 1", "side effect 2", etc.]
            "connected_diseases": ["Disease name 1", "Disease name 2"], 
            "connected_centers": ["Center name 1", "Center name 2"],
        }


TREATMENT CENTERS:

    SCHEMA:
        {
            "name": <CENTER NAME>,
            "attribute_state": <TWO LETTER STATE ABBREVIATION>,
            "attribute_capacity": <IINTEGER>,
            "attribute_class": <"Hospital", "Office", or "Center>,
            "attribute_branch": <"Rheumatology", "Lupus", etc.>
            "website": <WEBSITE LINK>,
            "city": <CITY>,
            "address": <CENTER ADDRESS>,
            "image": "<IMAGE LINK>",
            "coordinates": {
                "lat": 42.3362036,
                "lng": -71.1067408
            },
            "contact": "617-732-5500",
            "description": "The Brigham and Women\u2019s Hospital (BWH) Lupus Center is a comprehensive center committed to providing state of the art, collaborative care to all patients with systemic lupus erythematosus (lupus or SLE).",
            "connected_diseases": ["disease name 1", "disease name 2"],
            "connected_treatments": ["treatment name 1", "treatment name 2"]
            
        }
