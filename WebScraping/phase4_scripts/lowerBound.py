import json

with open('../diseases.json', 'r') as file:
    data = json.load(file)

for item in data:
    age_range = item['attribute_age_of_onset']
    lower_bound = int(age_range.split('-')[0])
    item['attribute_age_of_onset'] = lower_bound

with open('updated_data.json', 'w') as file:
    json.dump(data, file, indent=4)
