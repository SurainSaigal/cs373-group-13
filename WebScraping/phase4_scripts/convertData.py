import json

def convert_year_to_int(input_file, output_file):
    # Load the JSON data from the input file
    with open(input_file, 'r') as file:
        data = json.load(file)
    
    # Iterate through each object in the list
    for obj in data:
        # Check if the object has the key 'attribute_year_discovered'
        if 'attribute_year_discovered' in obj:
            # Convert the value from string to integer
            obj['attribute_year_discovered'] = int(obj['attribute_year_discovered'])
    
    # Write the modified list of objects to the new JSON file
    with open(output_file, 'w') as file:
        json.dump(data, file, indent=4)

# Example usage
input_file = '../diseases.json'
output_file = 'updated_data.json'
convert_year_to_int(input_file, output_file)
