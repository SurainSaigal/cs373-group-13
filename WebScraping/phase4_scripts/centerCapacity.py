import json
import random

def main():
    # Load the treatment centers JSON
    with open('../treatment_centers.json', 'r') as file:
        treatment_centers = json.load(file)

    # Update the capacity information
    updated_centers = []
    for center in treatment_centers:
        if center['attribute_capacity'] == 0:
            # Generate a random capacity between 500 and 5000
            capacity = random.randint(500, 5000)
        else:
            capacity = center['attribute_capacity']
        
        center['attribute_capacity'] = capacity
        updated_centers.append(center)
        # print(center['name'], capacity)

    # Save the updated JSON data back to a different file to ensure the original data is not overwritten
    with open('./updated_data.json', 'w') as file:
        json.dump(updated_centers, file, indent=4)

if __name__ == "__main__":
    main()
