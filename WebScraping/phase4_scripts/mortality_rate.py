import json
import random

def main():
    # Load the JSON data from a file
    with open('../diseases.json', 'r') as file:  # Update the file path
        data = json.load(file)

    # Update the mortality rate information
    updated_data = []
    for item in data:
        # Check if 'attribute_mortality_rate' exists in the object
        if 'attribute_mortality_rate' in item:
            # Generate a random mortality rate between 0.1% and 15%
            mortality_rate = round(random.uniform(0.001, 0.15), 4)  # 4 decimal places for precision
            item['attribute_mortality_rate'] = mortality_rate
        
        updated_data.append(item)

    # Save the updated JSON data back to a different file to ensure the original data is not overwritten
    with open('./updated_data.json', 'w') as file:  # Update the file path for output
        json.dump(updated_data, file, indent=4)

if __name__ == "__main__":
    main()
