import json
import random

def update_attribute_cost(input_file, output_file):
    # Load the JSON data from the input file
    with open(input_file, 'r') as file:
        data = json.load(file)
    
    # Iterate through each object in the list
    for obj in data:
        # Check if 'attribute_cost' is 0 and update it
        if obj.get("attribute_cost", 1) == 0:
            obj["attribute_cost"] = random.randint(60, 150)
    
    # Write the modified list of objects to the new JSON file
    with open(output_file, 'w') as file:
        json.dump(data, file, indent=4)

# Example usage
input_file = '../treatments.json'  # Update this path to your input file
output_file = './updated_data.json'  # Update this path to your desired output file
update_attribute_cost(input_file, output_file)
