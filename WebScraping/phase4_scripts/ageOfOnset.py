import json
import requests
from bs4 import BeautifulSoup


def get_age_of_onset(disease):
    url = f"https://en.wikipedia.org/wiki/{disease.replace(' ', '_')}"
    response = requests.get(url)

    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')

        # List of potential strings indicating age of onset information
        age_of_onset_strings = [
            'Age of onset', 
            'Onset age', 
            'Age at onset', 
            'Age when onset begins', 
            'Age of onset and epidemiology', 
            'Age at presentation', 
            'Typical onset', 
            'Age at first onset'
        ]

        # Search for age of onset information within page content
        page_content = soup.find('div', {'id': 'content'})
        if page_content:
            for age_of_onset_string in age_of_onset_strings:
                age_of_onset = page_content.find(string=age_of_onset_string)
                if age_of_onset:
                    # Navigate to the nearest table after the found string
                    age_of_onset_table = age_of_onset.find_next('table')
                    if age_of_onset_table:
                        age_of_onset = age_of_onset_table.text.strip()
                        return age_of_onset
        
        return "Age of onset information not found"
    else:
        return None



def main():
    input_file = '../names/disease.json'  # Input JSON file containing all disease names
    output_file = 'age_of_onset_data.json'  # Output JSON file to save age of onset data

    with open(input_file, 'r') as file:
        disease_names = json.load(file)

    age_of_onset_data = {}

    for disease_name in disease_names:
        age_of_onset = get_age_of_onset(disease_name)
        age_of_onset_data[disease_name] = age_of_onset

    # Save the age of onset data to a JSON file
    with open(output_file, 'w') as file:
        json.dump(age_of_onset_data, file, indent=4)

    print(f"Age of onset data saved to {output_file}")

if __name__ == "__main__":
    main()
