import json
import random

def is_valid_phone_number(phone_number):
    # Checks if the phone number matches the format ###-###-####
    if len(phone_number) != 12:
        return False
    parts = phone_number.split('-')
    if len(parts) != 3 or not all(part.isdigit() for part in parts):
        return False
    return True

def generate_phone_number():
    # Generates a random phone number in the format ###-###-####
    area_codes = ['212', '213', '312', '408', '415', '510', '619', '626', '650', '707', '714', '760', '805', '818', '858', '909', '916', '925', '949', '951']
    area_code = random.choice(area_codes)
    prefix = random.randint(100, 999)
    line_number = random.randint(1000, 9999)
    return f"{area_code}-{prefix}-{line_number}"

def modify_contact_numbers(input_file, output_file):
    # Load the JSON data from the input file
    with open(input_file, 'r') as file:
        data = json.load(file)
    
    # Iterate through each object in the list
    for obj in data:
        # Check and modify the 'contact' field if necessary
        contact = obj.get("contact", "")
        if contact == "999-999-9999" or not is_valid_phone_number(contact):
            obj["contact"] = generate_phone_number()
    
    # Write the modified list of objects to the new JSON file
    with open(output_file, 'w') as file:
        json.dump(data, file, indent=4)

# Replace 'input.json' and 'output.json' with your actual file paths
modify_contact_numbers('../treatment_centers.json', './updated_data.json')
