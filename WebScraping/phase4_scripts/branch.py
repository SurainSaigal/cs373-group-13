import json

def update_branch_information(input_file, output_file):
    """
    Update the branch information for each treatment center in the JSON file.
    Centers with 'attribute_branch' set to 'NA' will be updated to have a more descriptive message.
    """
    # Load the treatment centers JSON
    with open(input_file, 'r') as file:
        treatment_centers = json.load(file)

    # Iterate over each treatment center and update 'attribute_branch' if necessary
    for center in treatment_centers:
        if center.get('attribute_branch', '') == 'NA':
            center['attribute_branch'] = 'No Branch is associated with this treatment center'

    # Save the updated data to a new file
    with open(output_file, 'w') as file:
        json.dump(treatment_centers, file, indent=4)

if __name__ == "__main__":
    input_file = '../treatment_centers.json'  # Path to the original data file
    output_file = './updated_data.json'  # Path for saving the updated data
    update_branch_information(input_file, output_file)
