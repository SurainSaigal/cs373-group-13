import json

def update_json_with_txt(original_json_path, txt_path, updated_json_path):
    # Read the original JSON file
    with open(original_json_path, 'r') as json_file:
        data = json.load(json_file)
    
    # Read the .txt file and update the 'leading_cause' for each object in the JSON
    with open(txt_path, 'r') as txt_file:
        for index, line in enumerate(txt_file):
            # Ensure there's a corresponding object to update
            if index < len(data):
                data[index]['leading_cause'] = line.strip()
            else:
                print(f"Warning: More lines in {txt_path} than objects in {original_json_path}. Extra lines ignored.")
                break

    # Save the updated data to a new JSON file to avoid corrupting the original
    with open(updated_json_path, 'w') as updated_json_file:
        json.dump(data, updated_json_file, indent=4)

# Specify the original JSON, the .txt file with updated 'leading_cause' values, and the path for the updated JSON
original_json_path = '../diseases.json'  # Placeholder path, replace with actual path to the original JSON file
txt_path = './new_leading_causes.txt'
updated_json_path = 'diseases.json'

# Call the function to perform the update
update_json_with_txt(original_json_path, txt_path, updated_json_path)