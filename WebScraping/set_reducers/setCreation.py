import json
import os

def ensure_directory_exists(directory_path):
    """Ensure the directory exists, create it if it doesn't."""
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

def extract_unique_attribute_values(input_file, attribute):
    # Initialize an empty set to store unique attribute values
    unique_values = set()
    
    # Load the JSON data from the input file
    with open(input_file, 'r') as file:
        data = json.load(file)
    
    # Iterate through each object in the list to extract attribute values
    for obj in data:
        if attribute in obj:
            unique_values.add(obj[attribute])
    
    # Extract the base name of the input file (without extension) for use in the directory name
    input_file_base_name = os.path.splitext(os.path.basename(input_file))[0]
    
    # Create a directory for the input file if it doesn't already exist
    directory_path = os.path.join(os.getcwd(), input_file_base_name)
    ensure_directory_exists(directory_path)
    
    # Define the output file name based on the attribute and place it in the created directory
    output_file = os.path.join(directory_path, f"{attribute}_set.txt")
    
    # Write the unique attribute values to the output file
    with open(output_file, 'w') as file:
        for value in sorted(unique_values):  # Sort the set for readability
            file.write(f"{value}\n")
    
    print(f"Unique values of '{attribute}' have been written to {output_file}")

# Example usage
input_fileA = '../diseases.json'  # Update this path to your input file
input_fileB = '../treatment_centers.json'  # Update this path to your input file
input_fileC = '../treatments.json'  # Update this path to your input file

attributesA = ['attribute_type', 'attribute_rarity', 'leading_cause']
attributesB = ['attribute_state', 'city', 'attribute_branch', 'attribute_class']
attributesC = ['attribute_class', 'attribute_type', 'attribute_availability']

for attribute in attributesA:
    extract_unique_attribute_values(input_fileA, attribute)
for attribute in attributesB:
    extract_unique_attribute_values(input_fileB, attribute)
for attribute in attributesC:
    extract_unique_attribute_values(input_fileC, attribute)
