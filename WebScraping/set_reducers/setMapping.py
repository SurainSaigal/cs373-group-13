import json

def load_mapping(mapping_file):
    mapping = {}
    with open(mapping_file, 'r') as file:
        for line in file:
            original, mapped = line.strip().split(" | ")
            mapping[original] = mapped
    return mapping

def update_attributes(input_file, attributes, mappings):
    # Load input JSON
    with open(input_file, 'r') as file:
        data = json.load(file)

    # Update attributes based on mappings
    for item in data:
        for attribute in attributes:
            if attribute in item and item[attribute] in mappings[attribute]:
                item[attribute] = mappings[attribute][item[attribute]]

    # Save modified data to a new JSON file
    output_file = input_file.replace('.json', '_modified.json')
    with open(output_file, 'w') as file:
        json.dump(data, file, indent=4)

    print(f"Updated file saved as {output_file}")

# Example usage
input_fileA = '../diseases.json'  # Adjust path as needed
input_fileC = '../treatments.json'  # Adjust path as needed

# Load all mappings
mappings = {
    'attribute_type':           load_mapping('./_maps/attribute_type_set.txt'),
    'attribute_class':          load_mapping('./_maps/attribute_class_set.txt'),
    'attribute_availability':   load_mapping('./_maps/attribute_availability_set.txt'),
    'tattribute_type':          load_mapping('./_maps/tattribute_type_set.txt')
    # 'leading_cause': load_mapping('leading_cause_set.txt'),
}

attributesA = ['attribute_type']
attributesC = ['attribute_class', 'attribute_type', 'attribute_availability']

# Apply mappings to JSON files
update_attributes(input_fileA, attributesA, mappings)
update_attributes(input_fileC, attributesC, mappings)
