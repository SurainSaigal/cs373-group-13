import json

def process_json(input_file):
    # Load the input JSON file
    with open(input_file, 'r') as file:
        data = json.load(file)

    # List to store the 'leading_cause' from each object
    leading_causes = []

    # Open a single file for all 'leading_cause' strings
    with open('all_leading_causes.txt', 'w') as file:
        # Iterate through each object in the loaded data
        for obj in data:
            try:
                # Extract the 'leading_cause' using [] syntax
                leading_cause = obj['leading_cause']
            except KeyError:
                # If 'leading_cause' is not found, you could choose to write 'N/A' or continue
                leading_cause = 'N/A'  # Or use continue to skip this object

            # Append the 'leading_cause' to the list
            leading_causes.append(leading_cause)

            # Write the 'leading_cause' to the file, each on a new line
            file.write(leading_cause + '\n')

    # Save the list of 'leading_cause' to output.json
    with open('output.json', 'w') as file:
        json.dump(leading_causes, file)

# Replace 'your_input_file.json' with the actual input file name
process_json('../diseases.json')


# Replace 'your_input_file.json' with the actual input file name
process_json('../diseases.json')
