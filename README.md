# cs373-group-13

**Group Number**: 13

**Team Members**: Surain Saigal, Callum Longenecker, Kathy Cui, Jose Alvarez Loredo, Yingting Cao

**Phase Leaders**

-   Phase 1: Surain Saigal (responsibilities: frontend set up, docker deployment)
-   Phase 2: Callum Longenecker (responsibilities: database set up, backend api setup)
-   Phase 3: Kathy Cui (responsibilities: delegate tasks and ensure timeline)
-   Phase 4: Jose Alvarez Loredo (responsibilities: design and implement data visualizations)

**Estimated and Actual Work Times- Phase 4**

-   Callum Longenecker - estimated: 4 hours, actual: 5 hours
-   Surain Saigal - estimated: 3 hours, actual: 7 hours
-   Kathy Cui - estimated: 3 hours; actual: 5 hours
-   Jose Alvarez Loredo - estimated: 10 hours; actual: 10 hours
-   Yingting Cao - estimated: 10 hours actual: 16 hours

**Project Name**: Autoimmune Diseases

**Most recent Git SHA (phase 4)**: 7651d9165058fa71b50bf2991096ddcb36966209

**Pipeline**: https://gitlab.com/SurainSaigal/cs373-group-13/-/pipelines

**Website Link**: https://www.autoimmunediseasecenter.me/

**Backend API Link**: https://api.autoimmunediseasecenter.me/

**Technical Report**: https://docs.google.com/document/d/1yYsD8TfqXmQ1L8qLdV2fsXbgQcvkjt_09xmHmWXK18Q/edit?usp=drive_link

**API Documentation**: https://documenter.getpostman.com/view/32898971/2sA2r3Z69s

**YouTube Video**: https://www.youtube.com/watch?v=u3RF6P8gsU8

**IDB 4 Rubric**: https://docs.google.com/document/d/1zVPdLcr-nlaN-Xp4cvQ5LnpPatGEL3hJpRANJKnS8cg/edit?usp=sharing

**Proposed Project**: To raise awareness about autoimmune diseases and provide information on where and how to find treatment for anyone affected.

**Data Sources**:

-   https://www.summahealth.org/flourish/entries/2021/09/finding-pain-relief-for-your-autoimmune-disease
-   https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-021-04268-4
-   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4897593/
-   https://autoimmune.org/resource-center/finding-a-physician/
-   https://autoimmune.org/
-   https://www.ahn.org/patients-visitors/patients/financial-services/insurance-coverage/by-payor
-   https://www.autoimmuneinstitute.org/resources/autoimmune-disease-list/
-   https://developers.google.com/maps - RESTful API
-   https://open.cdc.gov/apis.html - RESTful API

**Models**:

-   Diseases
-   Treatments
-   Treatment Centers in the US

**Estimate of Number of Instances**:

-   Diseases ~100
-   Treatments ~150 (4/5 major categories of treatment, can be divided in to many sub categories)
-   Clinics/Treatment Centers ~50 in the US

**Model Attributes**:

-   Diseases:
    -   Disease Name (A-Z) - sortable
    -   Type (organ specific / organ agnostic) - sortable
    -   date discovered/researched - sortable
    -   Rarity / Commonality - sortable
    -   Patient Characteristics (age, sex, genes, etc.) - sortable
    -   General Summary
    -   Research (statistics, clinical trials, scientific studies/research)
    -   Expected lifespan
-   Treatment centers:

    -   Name (A-Z) (sortable)
    -   State (A-Z) (sortable)
    -   Branch
    -   Size/Capacity (sortable)
    -   Distance (sortable)
    -   Number of Insurance Companies accepted (sortable)

-   Treatments:
    -   Name (A-Z) (sortable)
    -   Type (OTC therapies, prescription drugs, lifestyle change, etc.) (sortable)
    -   Cost (sortable)
    -   Availability (from widely available to rare) (sortable)
    -   Success Rate (sortable)
    -   Insurance Type Required (sortable)
    -   category of treatment (painkillers, suppressants, etc.) (sortable)

**Connecting Models**:

-   An instance of a disease will have links to instances of treatment centers where that disease is treatable, and will have links to instances of treatments that are used for it
-   An instance of a treatment center will have links to diseases treated at the center and links to treatments offered at the center
-   An instance of a treatment will have links to diseases it is effective against and links to treatment centers that offer it

**Media Types For Models**:

-   Diseases:
    -   patient videos on treatment experience
    -   images of symptoms
-   Treatment centers in the US:
    -   Google Maps GPS locations
    -   Images of the facility
-   Treatments:
    -   image of drug/treatment process
    -   video for treatment/ a personal success story,
    -   brand logo

**Questions our site answers**:

-   What are some available treatment options?
-   Where can I find resources and centers that specialize in a specific disease?
-   What is an autoimmune disease and what are some different types?
