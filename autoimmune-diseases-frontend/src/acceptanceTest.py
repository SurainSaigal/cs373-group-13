import unittest
import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


HOME_PAGE_URL = "https://www.autoimmunediseasecenter.me/"
ABOUT_PAGE_URL = "https://autoimmunediseasecenter.me/about"


class TestGui(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--window-size=1920x1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        self.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install()))
        self.driver.maximize_window()
        self.driver.get(HOME_PAGE_URL)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def get_page_source(self, url):
        self.driver.get(url)
        # Add a wait for the page to load completely
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, 'body'))
        )
        return self.driver.page_source

    def test_home_URL(self):
        self.driver.get(HOME_PAGE_URL)
        self.assertEqual(self.driver.current_url, HOME_PAGE_URL)

    def test_about_URL(self):
        self.driver.get(ABOUT_PAGE_URL)
        self.assertEqual(self.driver.current_url, ABOUT_PAGE_URL)

    def test_disease_URL(self):
        extension = "disease"
        self.driver.get(HOME_PAGE_URL + extension)
        self.assertEqual(self.driver.current_url, HOME_PAGE_URL + extension)

    def test_treatment_URL(self):
        extension = "treatments"
        self.driver.get(HOME_PAGE_URL + extension)
        self.assertEqual(self.driver.current_url, HOME_PAGE_URL + extension)

    def test_resources_URL(self):
        extension = "treatment_center"
        self.driver.get(HOME_PAGE_URL + extension)
        self.assertEqual(self.driver.current_url, HOME_PAGE_URL + extension)

    def test_about_page_title_exists(self):
        page_source = self.get_page_source(ABOUT_PAGE_URL)
        self.assertIn("About Us", page_source)

    def test_about_page_team_section_exists(self):
        page_source = self.get_page_source(ABOUT_PAGE_URL)
        self.assertIn("The Team", page_source)

    def test_about_page_data_sources_exist(self):
        page_source = self.get_page_source(ABOUT_PAGE_URL)
        self.assertIn("Data Sources", page_source)

    def test_about_page_paragraph_sources_exist(self):
        page_source = self.get_page_source(ABOUT_PAGE_URL)
        self.assertIn("Integrating this disparate data has shown that many autoimmune diseases share common treatment options and centers, and that many diseases are even treatable via more than one method. We hope this wesbite can give anyone struggling a complete overview of their options.", page_source)

    def test_navigation_links_exist(self):
        nav_links = self.driver.find_elements(
            By.XPATH, "//ul[@class='navbar-nav']/li/a")
        expected_links = ["Home", "About", "Diseases",
                          "Treatments", "Treatment Centers"]
        for link, expected_text in zip(nav_links, expected_links):
            self.assertEqual(link.text, expected_text)

    def test_sort(self):
        self.driver.get("https://www.autoimmunediseasecenter.me/treatments")
        # self.driver.get("http://localhost:3000/treatments")
        sort_by_element = self.driver.find_element(
            By.XPATH, "//*[contains(text(), 'Sort by')]")
        self.assertTrue(sort_by_element.is_displayed())


if __name__ == "__main__":
    unittest.main()
