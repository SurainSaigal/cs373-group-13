import React from 'react';
import { render, screen } from '@testing-library/react';
import LinkCard from '../components/LinkCard.js';

describe('LinkCard', () => {
  test('renders card with link', () => {
    const name = 'Example Link';
    const type = 'exampleType';
    const external = true;

    render(<LinkCard name={name} type={type} external={external} />);

    const cardTitleElement = screen.getByText(name);
    expect(cardTitleElement).toBeInTheDocument();

    const linkElement = screen.getByRole('link', { name: name });
    expect(linkElement).toHaveAttribute('href', `/${type}/undefined`);
  });

  test('renders card without link', () => {
    const name = 'Example Link';
    const external = false;

    render(<LinkCard name={name} external={external} />);

    const cardTitleElement = screen.getByText(name);
    expect(cardTitleElement).toBeInTheDocument();

    const linkElement = screen.queryByRole('link', { name: name });
    expect(linkElement).toBeInTheDocument();
  });

  

});
