import React from 'react';
import { render } from '@testing-library/react';
import Home from '../app/home/page.js';

describe('Home Component', () => {
  it('renders without crashing', () => {
    render(<Home />);
  });

  it('renders welcome message', () => {
    const { getByText } = render(<Home />);
    const welcomeElement = getByText(/Welcome to the Autoimmune Disease Resource Center/i);
    expect(welcomeElement).toBeInTheDocument();
  });

  it('renders introductory text', () => {
    const { getByText } = render(<Home />);
    const introText = getByText(/Living with an autoimmune disease can be challenging/i);
    expect(introText).toBeInTheDocument();
  });

  it('renders "How To Help" section', () => {
    const { getByText } = render(<Home />);
    const helpSection = getByText(/How To Help/i);
    expect(helpSection).toBeInTheDocument();
  });

  it('renders "How To Help" text', () => {
    const { getByText } = render(<Home />);
    const helpSection = getByText(/Here are some ways you can support a loved one with an autoimmune disease:/i);
    expect(helpSection).toBeInTheDocument();
  });
});
