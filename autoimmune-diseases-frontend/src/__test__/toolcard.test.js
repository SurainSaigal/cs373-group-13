import React from 'react';
import { render, screen } from '@testing-library/react';
import ToolCard from '../components/ToolCard';

describe('ToolCard component', () => {
  const mockData = {
    toolName: 'Test Tool',
    imageSource: 'test.jpg',
    link: 'https://example.com'
  };

  test('renders tool name correctly', () => {
    render(<ToolCard {...mockData} />);
    const toolNameElement = screen.getByText('Test Tool');
    expect(toolNameElement).toBeInTheDocument();
  });

  test('renders link correctly', () => {
    render(<ToolCard {...mockData} />);
    const linkElement = screen.getByRole('link', { name: '' });
    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toHaveAttribute('href', 'https://example.com');
    expect(linkElement).toHaveAttribute('target', '_blank');
    expect(linkElement).toHaveAttribute('rel', 'noopener noreferrer');
  });
});