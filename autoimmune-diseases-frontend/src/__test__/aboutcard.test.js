import React from 'react';
import { render, screen } from '@testing-library/react';
import AboutCard from '../components/AboutCard';

// Unit test 1 test About card
describe('AboutCard Component', () => {
  const props = {
    firstName: 'Nicole',
    lastName: 'Cao',
    commits: 14,
    issues: 13,
    unit_tests: '0',
    bio: 'I am a junior studying computer science. I like drawing and painting during my free time.',
    responsibilities:'Frontend',
  };

  it('renders AboutCard component with correct props', () => {
    render(<AboutCard {...props} />);

    // Assert that the full name is rendered
    expect(screen.getByText('Nicole Cao')).toBeInTheDocument();

    // Assert that the bio is rendered
    expect(screen.getByText('I am a junior studying computer science. I like drawing and painting during my free time.')).toBeInTheDocument();

      // Assert that the responsibilities are rendered
    expect(screen.getByText('Responsibilities')).toBeInTheDocument();
    expect(screen.getByText(': Frontend')).toBeInTheDocument();

  } );
});
