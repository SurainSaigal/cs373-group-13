"use client";
import React from "react";
import BarChart2 from "./BarChart2/BarChart2";
import BubbleChart from "./BubbleChart/BubbleChart";
import DonutChart from "./DonutChart/DonutChart";

const Visual = () => {
    return (
        <div className="row justify-content-around">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                <h3 className="text-center mt-3 mb-3">{`Locations Frequency Chart (for frequencies > 50)`}</h3>
                <BarChart2 />
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mt-5">
                <h3 className="text-center mt-3">Therapists Race and Ethnicity Distribution</h3>
                <DonutChart />
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mb-5">
                <h3 className="text-center mt-3">Facilities Based on Rating</h3>
                <BubbleChart />
            </div>
        </div>
    );
};

export default Visual;
