import ids from '../currentT.json';
// const dataList = await fetchAllData(ids);
import dataList from '../therapists.json';

const fetchData = async (id) => {
  const apiUrl = `https://apitexastrauma.support/Therapists/id/${id.id}`;
  try {
    const response = await fetch(apiUrl);
    if (!response.ok) {
      throw new Error(`Failed to fetch data for ID: ${id.id}, Status: ${response.status}`);
    }
    const data = await response.json();
    return {
      name: data.name ?? "Unknown",
      gender: data.gender ?? "Unknown",
      raceEthnicity: data.raceEthnicity ?? "Unknown",
    };
  } catch (error) {
    console.error(`Error fetching data for ID: ${id.id}`, error);
    return null;
  }
};

const fetchAllData = async (ids) => {
  const dataList = [];
  for (const entry of ids) {
    const dataItem = await fetchData(entry);
    if (dataItem) {
      dataList.push(dataItem);
    }
  }
  return dataList;
};

const ensureData = (rawData) => {
  return rawData.map((item) => ({
    name: item.name || "Unknown",
    gender: item.gender || "Unknown",
    raceEthnicity: item.raceEthnicity || "Unknown",
  }));
};

const groupSingleCounts = (data) => {
  const counts = new Map();
  data.forEach((item) => {
    const raceEthnicity = item.raceEthnicity || "Unknown";
    counts.set(raceEthnicity, (counts.get(raceEthnicity) || 0) + 1);
  });

  return data.map((item) => {
    const raceEthnicity = item.raceEthnicity ?? "Unknown";
    const count = counts.get(raceEthnicity) ?? 0;
    if (count < 4) {
      return { ...item, raceEthnicity: "one or more races" };
    }
    return item;
  });
};

export const therapistsData = groupSingleCounts(ensureData(dataList));