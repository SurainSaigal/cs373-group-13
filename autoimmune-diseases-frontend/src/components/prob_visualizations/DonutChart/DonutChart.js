import React, { useEffect, useRef } from 'react';
import { setupDonutChart } from './D3Setup';
import { therapistsData } from './Data';

const DonutChart = () => {
  const svgRef = useRef(null);

  useEffect(() => {
    const cleanup = setupDonutChart(svgRef.current, therapistsData);

    return cleanup;
  }, []);

  return <svg ref={svgRef}></svg>;
};

export default DonutChart;