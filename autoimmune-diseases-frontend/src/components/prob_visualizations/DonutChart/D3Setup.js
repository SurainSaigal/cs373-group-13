import * as d3 from "d3";
import { DataItem } from "./Data";

export const setupDonutChart = (svgElement, data) => {
    if (!svgElement) {
        return;
    }

    const width = 900;
    const height = 500;
    const radius = Math.min(width, height) / 2;
    const color = d3.scaleOrdinal(d3.schemeCategory10);

    const svg = d3.select(svgElement);
    svg.selectAll("*").remove();

    const g = svg
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", `translate(${width / 2}, ${height / 2})`);

    const raceEthnicityCounts = d3.rollup(
        data,
        (v) => v.length,
        (d) => d.raceEthnicity || "Not specified"
    );

    const pie = d3.pie().value((d) => d[1]);
    const arc = d3
        .arc()
        .innerRadius(radius - 120)
        .outerRadius(radius - 20);

    const pieData = pie([...raceEthnicityCounts.entries()]);
    const arcs = g.selectAll(".arc").data(pieData).enter();

    arcs.append("path")
        .attr("class", "arc")
        .attr("d", arc)
        .attr("fill", (d) => color(d.data[0]))
        .attr("stroke", "white")
        .attr("stroke-width", 2)
        .append("title")
        .text((d) => `${d.data[0]}: ${d.data[1]}`);

    arcs.append("rect").attr(
        "transform",
        (d) => `translate(${arc.centroid(d)[0] * 1.0}, ${arc.centroid(d)[1] * 1.0})`
    );

    arcs.append("text")
        .attr(
            "transform",
            (d) => `translate(${arc.centroid(d)[0] * 1.0}, ${arc.centroid(d)[1] * 1.0})`
        )
        .attr("text-anchor", "middle")
        .attr("dominant-baseline", "middle")
        .style("fill", "black")
        .text((d) => d.data[0]);

    // Remove the labels
    arcs.selectAll("text").remove();

    // Create the legend
    const legend = svg
        .selectAll(".legend")
        .data(pieData)
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", (d, i) => `translate(${width / 2 + radius - 15}, ${i * 20 + 180})`);

    legend
        .append("rect")
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", (d) => color(d.data[0]));

    legend
        .append("text")
        .attr("x", 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "start")
        .text((d) => d.data[0]);
};
