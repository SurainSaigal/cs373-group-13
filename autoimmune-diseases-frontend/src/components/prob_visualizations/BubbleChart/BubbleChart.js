// const dataItems = await fetchAllData(ids);;
import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import ids from "../currentF.json";
import dataList from "../Facility.json";

// Fetch data for a given ID
const fetchData = async (id) => {
    const apiUrl = `https://apitexastrauma.support/Facilities/id/${id.id}`;
    try {
        const response = await fetch(apiUrl);
        if (!response.ok) {
            throw new Error(`Failed to fetch data for ID: ${id.id}, Status: ${response.status}`);
        }
        const data = await response.json();
        return {
            facilityName: data.facilityName || "Unknown",
            reviewRating: data.reviewRating || "Unknown",
        };
    } catch (error) {
        console.error(`Error fetching data for ID: ${id.id}`, error);
        return null;
    }
};

// Fetch data for all IDs
const fetchAllData = async (ids) => {
    const dataList = [];
    for (const id of ids) {
        const data = await fetchData(id);
        if (data) {
            dataList.push(data);
        }
    }
    return dataList;
};

// Clean and limit the dataset
const cleanAndLimitData = (rawData, limit = 20) => {
    return rawData
        .filter((item) => item.reviewRating !== "Unknown")
        .map((item) => ({
            facilityName: item.facilityName,
            reviewRating: item.reviewRating,
        }))
        .slice(0, limit);
};

const cleanedData = cleanAndLimitData(dataList, 20);

const rootData = { children: cleanedData };

// Component for D3 Bubble Chart
const BubbleChart = () => {
    const ref = useRef(null);

    useEffect(() => {
        const svg = d3.select(ref.current);
        const width = 600;
        const height = 600;
        const margin = 10;

        const pack = d3
            .pack()
            .size([width - 2 * margin, height - 2 * margin])
            .padding(5);

        const root = d3.hierarchy(rootData).sum((d) => parseFloat(d.reviewRating));
        const packedData = pack(root);

        svg.attr("width", width)
            .attr("height", height)
            .attr("viewBox", [-margin, -margin, width, height])
            .style("font", "10px sans-serif")
            .attr("text-anchor", "middle");

        const group = svg.append("g");

        const nodes = group
            .selectAll("g")
            .data(packedData.leaves())
            .join("g")
            .attr("transform", (d) => `translate(${d.x}, ${d.y})`);

        nodes
            .append("circle")
            .attr("r", (d) => d.r)
            .attr("fill", (d) => d3.scaleOrdinal(d3.schemeTableau10)(d.data.facilityName))
            .attr("fill-opacity", 0.7)
            .attr("stroke", "white")
            .attr("stroke-width", 1);

        nodes.append("title").text((d) => `${d.data.facilityName}\nRating: ${d.data.reviewRating}`);

        nodes
            .append("text")
            .attr("y", 4)
            .style("font-size", "8px")
            .each(function (d) {
                const el = d3.select(this);
                el.append("tspan").text(d.data.facilityName);
                el.append("tspan").attr("x", 0).attr("dy", "1.2em").text(d.data.reviewRating);
            });

        return () => {
            group.remove();
        };
    }, []);

    return <svg ref={ref} width={600} height={600} />;
};

export default BubbleChart;
