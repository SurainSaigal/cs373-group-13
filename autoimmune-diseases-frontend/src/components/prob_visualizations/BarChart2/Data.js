// const dataItems = await fetchAllData(ids);
import ids from '../currentSG.json';
import dataItems from "../SupportGroup.json";

const fetchData = async (id) => {
    const apiUrl = `https://apitexastrauma.support/SupportGroups/id/${id.id}`;
    try {
        const response = await fetch(apiUrl);
        if (!response.ok) {
            throw new Error(`Failed to fetch data for ID: ${id.id}, Status: ${response.status}`);
        }
        const data = await response.json();
        return {
            sessionHostName: data.sessionHostName ?? "Unknown",
            location: data.location ?? "Unknown",
        };
    } catch (error) {
        console.error(`Error fetching data for ID: ${id.id}`, error);
        return null;
    }
};

const fetchAllData = async (ids) => {
    const dataList = [];
    for (const entry of ids) {
        const dataItem = await fetchData(entry);
        if (dataItem) {
            dataList.push(dataItem);
        }
    }
    return dataList;
};

const processRawData = (rawData) => {
    return rawData.map((item) => ({
        sessionHostName: item.sessionHostName ?? "Unknown",
        location: item.location ?? "Unknown",
    }));
};

const data = processRawData(dataItems);

const countAndFilterLocations = (data, threshold) => {
    const locationCounts = {};

    data.forEach((item) => {
        const location = item.location;
        locationCounts[location] = (locationCounts[location] || 0) + 1;
    });

    return Object.keys(locationCounts)
        .map((location) => ({
            location,
            count: locationCounts[location],
        }))
        .filter((item) => item.count >= threshold);
};

const threshold = 50;

export const locationCounts = countAndFilterLocations(data, threshold);
