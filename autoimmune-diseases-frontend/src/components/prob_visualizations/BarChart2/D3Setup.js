import * as d3 from "d3";
import { locationCounts } from "./Data";

export const setupD3BarChart = (svgElement) => {
    const svg = d3.select(svgElement);

    const width = 600;
    const height = 400;
    const legendWidth = 120;

    svg.selectAll("*").remove();

    const xScale = d3
        .scaleBand()
        .domain(locationCounts.map((item) => item.location))
        .range([0, width - legendWidth])
        .padding(0.4);

    const yScale = d3
        .scaleLinear()
        .domain([0, d3.max(locationCounts, (item) => item.count) ?? 0])
        .range([height, 0]);

    const colorScale = d3
        .scaleOrdinal()
        .domain(locationCounts.map((item) => item.location))
        .range(["steelblue", "orange", "green", "purple", "red", "yellow", "pink", "brown"]);

    svg.selectAll("rect.bar")
        .data(locationCounts)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", (item) => xScale(item.location) ?? 0)
        .attr("y", (item) => yScale(item.count) ?? 0)
        .attr("width", xScale.bandwidth() ?? 0)
        .attr("height", (item) => height - (yScale(item.count) ?? 0))
        .attr("fill", (item) => colorScale(item.location));

    svg.selectAll("text.bar-label")
        .data(locationCounts)
        .enter()
        .append("text")
        .attr("class", "bar-label")
        .attr("x", (item) => (xScale(item.location) ?? 0) + (xScale.bandwidth() ?? 0) / 2)
        .attr("y", (item) => yScale(item.count) + 20 ?? 0)
        .attr("text-anchor", "middle")
        .style("fill", "black")
        .text((item) => item.count.toString());

    const legendGroup = svg
        .append("g")
        .attr("class", "legend")
        .attr("transform", `translate(${width - legendWidth}, 20)`);

    locationCounts.forEach((item, index) => {
        legendGroup
            .append("rect")
            .attr("x", 0)
            .attr("y", index * 30)
            .attr("width", 20)
            .attr("height", 20)
            .attr("fill", colorScale(item.location));

        legendGroup
            .append("text")
            .attr("x", 30)
            .attr("y", index * 30 + 15)
            .style("fill", "gray")
            .text(item.location);
    });

    svg.append("g").attr("transform", `translate(0, ${height})`).call(d3.axisBottom(xScale));

    svg.append("g").call(d3.axisLeft(yScale));
};
