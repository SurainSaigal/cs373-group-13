import { FaExternalLinkAlt } from "react-icons/fa";
const LinkCard = ({ name, type, id }) => {
    if (name.length > 45) {
        name = name.substring(0, 45) + "...";
    }
    const link = `/${type}/${id}`;

    let bgrnd = "";
    let border = "";
    if (type === "diseases") {
        bgrnd = "#34d0ff";
        border = "#01c4ff";
    } else if (type === "treatments") {
        bgrnd = "#84e599";
        border = "#65df80";
    } else if (type === "treatment-centers") {
        bgrnd = "#80f3ec";
        border = "#00E7D9";
    }

    return (
        <a href={link} style={{ textDecoration: "none" }}>
            <div
                className="card d-flex align-items-center justify-content-center card-hover-effect"
                style={{
                    width: "10rem",
                    height: "6rem",
                    backgroundColor: bgrnd,
                    border: `3px solid ${border}`,
                }}
            >
                <p className="card-title text-center mb-0">
                    {name}
                    <FaExternalLinkAlt className="ms-2" color="blue" />
                </p>
            </div>
        </a>
    );
};

export default LinkCard;
