"use client";
import { APIProvider, Map, AdvancedMarker } from "@vis.gl/react-google-maps";
import React, { useState, useCallback } from "react";

export default function CenterMap({ lat, lng }) {
    const api_key = "AIzaSyB45gRh6nVLgyLb0Zbq711-XmI3440pcn0";
    const google_map_id = "98639966b4992d19";
    const position = { lat: lat, lng: lng };

    const INITIAL_CAMERA = {
        center: { lat: lat, lng: lng },
        zoom: 15,
    };

    const [cameraProps, setCameraProps] = useState(INITIAL_CAMERA);
    const handleCameraChange = useCallback((ev) => setCameraProps(ev.detail));
    return (
        <APIProvider apiKey={api_key}>
            <div className="responsive-iframe">
                <Map mapId={google_map_id} {...cameraProps} onCameraChanged={handleCameraChange}>
                    <AdvancedMarker position={position}></AdvancedMarker>
                </Map>
            </div>
        </APIProvider>
    );
}
