const ToolCard = ({ toolName, imageSource, link }) => {
    return (
        <div className="text-center">
            <a href={link} target="_blank" rel="noopener noreferrer">
                <div
                    className="card border-light justify-content-center card-hover-effect"
                    style={{ width: "12rem", height: "12rem" }}
                >
                    <img src={imageSource} className="card-img-top" />
                </div>
            </a>
            <p className="mt-1">
                <strong>{toolName}</strong>
            </p>
        </div>
    );
};

export default ToolCard;
