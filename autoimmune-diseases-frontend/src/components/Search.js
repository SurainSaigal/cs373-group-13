"use client";
import React, { useEffect, useState } from "react";
import ModelCard from "@/components/ModelCard.js";
import { getAttributes } from "@/components/GetAttributes.js";
import { backendUrl } from "@/helpers/CallBackend";
import { FaSearch } from "react-icons/fa";

const Search = (props) => {
    useEffect(() => {
        async function getLoader() {
            const { helix } = await import("ldrs");
            helix.register();
        }
        getLoader();
    }, []);
    const type = props.type;
    const sortable = props.sortable;
    const filterable = props.filterable;
    const filter_types = Object.keys(filterable);

    const frontendType = type.replace("_", "-");

    const [pg, setPg] = useState(1);
    const [first, setFirst] = useState(0);
    const [last, setLast] = useState(0);
    const [cards, setCards] = useState([]);
    const [total, setTotal] = useState(0);
    const [last_page, setLastPage] = useState(0);
    const [loading, setLoading] = useState(true);
    const [searchTerm, setSearchTerm] = useState("");
    const [sortBy, setSortBy] = useState(sortable[0]);
    const [filters, setFilters] = useState([]);
    const [sortOrder, setSortOrder] = useState("asc");
    const [filterNum, setFilterNum] = useState(0);
    const [noSort, setNoSort] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        const inputValue = event.target.querySelector('input[type="search"]').value;
        if (inputValue !== searchTerm) {
            setSearchTerm(inputValue);
        }

        if (inputValue === "") {
            setNoSort(false);
        } else {
            setNoSort(true);
        }
    };

    const handleSelectChange = (event) => {
        const [sortBy, sortOrder] = event.target.value.split(":");
        setSortBy(sortBy);
        setSortOrder(sortOrder);
    };

    const handleFilterChange = (event) => {
        let filt = event.target.value.replace("attribute_", "").replaceAll(" ", "_");
        let new_filters = [...filters];
        if (event.target.checked) {
            new_filters.push(filt);
        } else {
            new_filters = new_filters.filter((filter) => filter !== filt);
        }
        setFilterNum(new_filters.length);
        setFilters(new_filters);
    };

    useEffect(() => {
        setLoading(true);
        const search = searchTerm ? `&search=${searchTerm.replaceAll(" ", "_")}` : "";
        let url = `${backendUrl}/v2/${type}?page=${pg}&per_page=12${search}`;
        if (!noSort) {
            url += `&sort_on=${sortBy}&sort_order=${sortOrder}`;
        }

        if (filters.length > 0) {
            url += "&";
            filters.forEach((filter) => {
                const [f, v] = filter.split(":");
                url += `filter_on_${f}=${v}&`;
            });
            url = url.slice(0, -1);
        }
        const fetchData = () => {
            fetch(url)
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    setCards(data.results);
                    const total_res = data[`total_${type}s`];
                    setLastPage(Math.ceil(total_res / 12));

                    setTotal(total_res);
                    setFirst((pg - 1) * 12 + 1);
                    setLast(pg * 12 > total_res ? total_res : pg * 12);
                    setLoading(false);
                });
        };

        fetchData();
    }, [pg]);

    useEffect(() => {
        setPg(1);
        setLoading(true);
        const search = searchTerm ? `&search=${searchTerm.replaceAll(" ", "_")}` : "";
        let url = `${backendUrl}/v2/${type}?page=1&per_page=12${search}&sort_on=${sortBy}&sort_order=${sortOrder}`;
        if (filters.length > 0) {
            url += "&";
            filters.forEach((filter) => {
                const [f, v] = filter.split(":");
                url += `filter_on_${f}=${v}&`;
            });
            url = url.slice(0, -1);
        }
        const fetchData = () => {
            fetch(url)
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    setCards(data.results);
                    const total_res = data[`total_${type}s`];
                    let page = total_res === 0 ? 0 : 1;
                    setLastPage(total_res != 0 ? Math.ceil(total_res / 12) : 1);
                    setTotal(total_res);
                    setFirst(page > 0 ? 1 : 0);

                    setLast(page * 12 > total_res ? total_res : page * 12);
                    setLoading(false);
                });
        };

        fetchData();
    }, [searchTerm, sortBy, sortOrder, filters]);

    return (
        <div className="">
            <div className="input-group flex-column flex-md-row justify-content-center">
                <div className="me-2 mt-2">Sort by</div>
                <div className="col-12 col-md-2 mb-3 mb-md-0">
                    <select className="form-select" onChange={handleSelectChange}>
                        {noSort ? (
                            <option value="none">Relevance</option>
                        ) : (
                            sortable.map((attr, index) => {
                                const alphabetic =
                                    attr === "name" ||
                                    attr === "attribute_state" ||
                                    attr === "attribute_branch";
                                const orderUpText = alphabetic ? "A-Z" : "Low-High";
                                const orderDownText = alphabetic ? "Z-A" : "High-Low";
                                const read = attr
                                    .replaceAll("attribute", "")
                                    .split("_")
                                    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                                    .join(" ");
                                return (
                                    <React.Fragment key={index}>
                                        <option value={attr + ":asc"} key={attr + ":asc"}>
                                            {read} ({orderUpText})
                                        </option>
                                        <option value={attr + ":desc"} key={attr + ":desc"}>
                                            {read} ({orderDownText})
                                        </option>
                                    </React.Fragment>
                                );
                            })
                        )}
                    </select>
                </div>

                <div className="col-12 col-md-4 mb-md-0 ms-md-4">
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-11 d-flex justify-content-center align-items-center">
                                <input
                                    type="search"
                                    className="form-control rounded"
                                    placeholder="Search"
                                    aria-label="Search"
                                    aria-describedby="search-addon"
                                />
                            </div>
                            <div className="col-1 d-flex justify-content-center align-items-center">
                                <button type="submit" className="btn btn-primary">
                                    <FaSearch color="white" />
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div className="ms-md-5 me-2 mt-2">Filters</div>

                <div className="dropdown col-12 col-md-3 mb-3 mb-md-0">
                    <button
                        className="btn dropdown-toggle border w-100"
                        type="button"
                        id="multiSelectDropdown"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                    >
                        {filterNum > 0 ? filterNum + " selected" : "Select"}
                    </button>
                    <form name="checkboxes">
                        <ul
                            className="dropdown-menu"
                            aria-labelledby="multiSelectDropdown"
                            style={{ width: "300px" }}
                        >
                            <li className="text-center" key="bt">
                                <button
                                    className="btn w-75"
                                    style={{ transition: "background-color 0.3s ease" }}
                                    onMouseOver={(e) =>
                                        (e.currentTarget.style.backgroundColor = "#D3D3D3")
                                    }
                                    onMouseOut={(e) => (e.currentTarget.style.backgroundColor = "")}
                                >
                                    Clear All
                                </button>
                            </li>

                            {filter_types.map((filter, index) => {
                                const read = filter
                                    .replaceAll("attribute", "")
                                    .split("_")
                                    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                                    .join(" ");
                                return (
                                    <div className="col" key={index}>
                                        <div className="col-6 ms-2">
                                            <strong>{read}</strong>
                                        </div>
                                        {filterable[filter].map((attr, attrIndex) => {
                                            return (
                                                <div className="col-12 ms-2" key={attrIndex}>
                                                    <li key="bt">
                                                        <label>
                                                            <input
                                                                type="checkbox"
                                                                value={filter + ":" + attr}
                                                                key={attrIndex}
                                                                onChange={handleFilterChange}
                                                                className="me-1"
                                                            />
                                                            {attr}
                                                        </label>
                                                    </li>
                                                </div>
                                            );
                                        })}
                                    </div>
                                );
                            })}
                        </ul>
                    </form>
                </div>
            </div>

            {loading ? (
                <div className="text-center mt-5">
                    <l-helix size="45" speed="2.5" color="black"></l-helix>
                </div>
            ) : (
                <div>
                    <div className="mt-3">
                        <p className="text-center fst-italic text-secondary">
                            Showing results {first} - {last} of {total}
                        </p>
                    </div>
                    <div className="row">
                        {cards.map((card, index) => {
                            const attributes = getAttributes(card);
                            const img_url =
                                type !== "treatment_center"
                                    ? card.image
                                    : `https://maps.googleapis.com/maps/api/staticmap?zoom=14&size=400x250&key=AIzaSyB45gRh6nVLgyLb0Zbq711-XmI3440pcn0&maptype=terrain&markers=color:0xFFFF00%7C${card.coordinates.lat},${card.coordinates.lng}`;
                            return (
                                <div
                                    className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center text-center mb-4"
                                    key={index}
                                >
                                    <ModelCard
                                        key={index}
                                        diseaseName={card.name}
                                        attributes={attributes}
                                        linkname={card.id}
                                        type={`${frontendType}s`}
                                        searchTerm={searchTerm}
                                        image={img_url}
                                    />
                                </div>
                            );
                        })}
                        <p
                            className="text-center fst-italic text-secondary"
                            style={{ marginBottom: "85px" }}
                        >
                            {" "}
                            Page {pg} of {last_page}
                        </p>
                    </div>
                    <nav aria-label="Pagination" className="fixed-bottom mb-3">
                        <ul className="pagination justify-content-center">
                            <li className={`page-item ${pg == 1 ? "disabled" : ""}`}>
                                <button className="page-link" onClick={() => setPg(1)}>
                                    {"<<"}
                                </button>
                            </li>
                            <li className={`page-item ${pg == 1 ? "disabled" : ""}`}>
                                <button className="page-link" onClick={() => setPg(pg - 1)}>
                                    Prev
                                </button>
                            </li>
                            <li className="page-item active" aria-current="page">
                                <span className="page-link">{pg}</span>
                            </li>
                            <li className={`page-item ${pg == last_page ? "disabled" : ""}`}>
                                <button className="page-link" onClick={() => setPg(pg + 1)}>
                                    Next
                                </button>
                            </li>
                            <li className={`page-item ${pg == last_page ? "disabled" : ""}`}>
                                <button className="page-link" onClick={() => setPg(last_page)}>
                                    {">>"}
                                </button>
                            </li>
                        </ul>
                    </nav>
                </div>
            )}
        </div>
    );
};

export default Search;
