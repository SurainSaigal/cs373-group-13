"use client";

import { useEffect } from "react";

function BootstrapClient() {
    useEffect(() => {
        require("bootstrap/dist/js/bootstrap.bundle.min.js");
        require("jquery/dist/jquery.slim");
    }, []);

    return null;
}

export default BootstrapClient;
