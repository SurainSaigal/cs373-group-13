"use client";
import { usePathname, useRouter } from "next/navigation";
import { Cabin } from "next/font/google";

const cabin = Cabin({
    weight: "600",
    subsets: ["latin"],
});

const NavBar = () => {
    const router = useRouter();
    const path = usePathname().split("/")[1];
    const handleSubmit = (e) => {
        e.preventDefault();
        const searchTerm = e.target.elements[0].value;
        router.push(`/search?term=${searchTerm}`);
    };
    return (
        <>
            <nav
                className="navbar navbar-expand-lg navbar-light fixed-top"
                style={{ backgroundColor: "#9FE2BF" }}
            >
                <div className={`container-fluid ${cabin.className}`}>
                    <a className={`navbar-brand`} href="/home">
                        Autoimmune Disease Resource Center
                    </a>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 me-3 mb-lg-0">
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${
                                        path === "home"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="/home"
                                >
                                    Home
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${
                                        path === "about"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="/about"
                                >
                                    About
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${
                                        path === "diseases"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="/diseases"
                                >
                                    Diseases
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${
                                        path === "treatments"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="/treatments"
                                >
                                    Treatments
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${
                                        path === "treatment-centers"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="/treatment-centers"
                                >
                                    Treatment Centers
                                </a>
                            </li>
                            <li className="nav-item dropdown">
                                <a
                                    className={`nav-link dropdown-toggle ${
                                        path === "visualizations" ||
                                        path === "provider-visualizations"
                                            ? "mt-1 border bg-primary border-primary text-white rounded p-1"
                                            : ""
                                    }`}
                                    href="#"
                                    id="navbarDropdown"
                                    role="button"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false"
                                >
                                    Visualizations
                                </a>
                                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a className="dropdown-item" href="/visualizations">
                                            Our Visualizations
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            className="dropdown-item"
                                            href="/provider-visualizations"
                                        >
                                            Provider Visualizations
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div className="col-12 col-sm-8 col-md-6 col-lg-4">
                            <form onSubmit={handleSubmit}>
                                <input
                                    type="search"
                                    className="form-control rounded"
                                    placeholder="Search"
                                    aria-label="Search"
                                    aria-describedby="search-addon"
                                />
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
        </>
    );
};

export default NavBar;
