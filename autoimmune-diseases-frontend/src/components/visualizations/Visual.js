"use client";
import React from "react";
import PieChart from "./PieChart/PieChart";
import ScatterPlot from "./ScatterPlot/ScatterPlot";
import BarChart from "./BarChart/BarChart";

const Visual = () => {
    return (
        <div className="row justify-content-center">
            <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                <h3 className="text-center mt-3 mb-3">Disorder Type Distribution</h3>
                <PieChart />
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mt-5">
                <h3 className="text-center mt-3">
                    Treatment Center Coordinate Distribution (x: latitude, y: longitude)
                </h3>
                <ScatterPlot />
            </div>
            <div className="col-12 d-flex flex-column justify-content-center align-items-center mb-5">
                <h3 className="text-center mt-3">Treatment Availability Distribution</h3>
                <BarChart />
            </div>
        </div>
    );
};

export default Visual;
