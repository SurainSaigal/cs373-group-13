import * as d3 from 'd3';

const fetchData = async () => {
  const response = await fetch('https://api.autoimmunediseasecenter.me/treatment');
  if (!response.ok) {
    throw new Error('Failed to fetch data');
  }
  return response.json();
};

export const getAttributeAvailabilityCount = async () => {
  const data = await fetchData();

  const attributeCounts = Array.from(
    d3.rollup(
      data,
      (v) => v.length,
      (d) => d.attribute_availability
    )
  );

  const otherThreshold = 5; // Group attributes with counts fewer than 4
  let otherCount = 0;

  const filteredCounts = attributeCounts.filter(([key, count]) => {
    if (count < otherThreshold) {
      otherCount += count;
      return false;
    }
    return true;
  });

  if (otherCount > 0) {
    filteredCounts.push(['other', otherCount]);
  }

  return filteredCounts;
};