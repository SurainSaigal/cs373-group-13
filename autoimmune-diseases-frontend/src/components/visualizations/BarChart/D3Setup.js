import * as d3 from "d3";
import { getAttributeAvailabilityCount } from "./Data";

export const setupD3BarChart = async (svgElement) => {
    const svg = d3.select(svgElement);
    const width = 600;
    const height = 400;
    const legendWidth = 120;

    svg.selectAll("*").remove();

    const attributeAvailabilityCount = await getAttributeAvailabilityCount();

    const xScale = d3
        .scaleBand()
        .domain(attributeAvailabilityCount.map(([availability]) => availability))
        .range([0, width - legendWidth])
        .padding(0.4);

    const yScale = d3
        .scaleLinear()
        .domain([0, d3.max(attributeAvailabilityCount, ([, count]) => count)])
        .range([height, 0]);

    const colorScale = d3
        .scaleOrdinal()
        .domain(attributeAvailabilityCount.map(([availability]) => availability))
        .range(["steelblue", "orange", "green", "purple", "red", "yellow", "pink", "brown"]);

    svg.selectAll("rect")
        .data(attributeAvailabilityCount)
        .enter()
        .append("rect")
        .attr("x", ([availability]) => xScale(availability))
        .attr("y", ([, count]) => yScale(count))
        .attr("width", xScale.bandwidth())
        .attr("height", ([, count]) => height - yScale(count))
        .attr("fill", ([availability]) => colorScale(availability));

    svg.selectAll("text.bar-label")
        .data(attributeAvailabilityCount)
        .enter()
        .append("text")
        .attr("class", "bar-label")
        .attr("x", ([availability]) => xScale(availability) + xScale.bandwidth() / 2)
        .attr("y", ([, count]) => yScale(count) + 20) // Adjust the y attribute of the labels
        .attr("text-anchor", "middle")
        .style("fill", "black")
        .text(([, count]) => count);

    const legendGroup = svg
        .append("g")
        .attr("class", "legend")
        .attr("transform", `translate(${width - legendWidth}, 20)`);

    attributeAvailabilityCount.forEach(([availability], index) => {
        legendGroup
            .append("rect")
            .attr("x", 0)
            .attr("y", index * 30)
            .attr("width", 20)
            .attr("height", 20)
            .attr("fill", colorScale(availability));

        legendGroup
            .append("text")
            .attr("x", 30)
            .attr("y", index * 30 + 15)
            .style("fill", "gray")
            .text(availability);
    });

    svg.append("g").attr("transform", `translate(0, ${height})`).call(d3.axisBottom(xScale));

    svg.append("g").call(d3.axisLeft(yScale));
};
