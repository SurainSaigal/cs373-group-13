"use client";

import React, { useEffect, useRef } from "react";
import { setupD3BarChart } from "./D3Setup";

const BarChart = () => {
    const svgRef = useRef(null);

    useEffect(() => {
        if (svgRef.current) {
            setupD3BarChart(svgRef.current);
        }
    }, []);

    return <svg ref={svgRef} width={720} height={400} />;
};

export default BarChart;
