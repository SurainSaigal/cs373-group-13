import * as d3 from "d3";

export const setupD3PieChart = (svg, attributeCounts) => {
    const width = 1200;
    const height = 600;
    const radius = Math.min(width, height) / 2;
    const color = d3.scaleOrdinal(d3.schemeCategory10);

    svg.attr("width", width).attr("height", height).selectAll("*").remove();

    const g = svg.append("g").attr("transform", `translate(${width / 2}, ${height / 2})`);

    const pie = d3.pie().value((d) => d.value);
    const arc = d3.arc().outerRadius(radius).innerRadius(0);

    const pieData = pie(
        Object.entries(attributeCounts).map(([key, value]) => ({
            key,
            value,
        }))
    );

    const arcs = g.selectAll(".arc").data(pieData).enter();

    arcs.append("path")
        .attr("class", "arc")
        .attr("d", arc)
        .attr("fill", (d) => color(d.data.key))
        .attr("stroke", "white")
        .attr("stroke-width", 2);

    // Create the legend
    const legend = svg
        .selectAll(".legend")
        .data(pieData)
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", (d, i) => `translate(${width / 2 + radius + 50}, ${i * 20 + 200})`);

    legend
        .append("rect")
        .attr("x", 0)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", (d) => color(d.data.key));

    legend
        .append("text")
        .attr("x", 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "start")
        .text((d) => d.data.key);
};
