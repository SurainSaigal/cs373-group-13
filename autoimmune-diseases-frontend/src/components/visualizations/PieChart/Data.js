export const fetchAttributeCounts = async () => {
    const response = await fetch('https://api.autoimmunediseasecenter.me/disease');
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }

    const data = await response.json();
    
    const attributeCounts = data.reduce((acc, item) => {
        acc[item.attribute_type] = (acc[item.attribute_type] || 0) + 1;
        return acc;
    }, {});

    const processedCounts = {};
    let otherCount = 0;

    for (const [key, count] of Object.entries(attributeCounts)) {
        if (count < 4) {
            otherCount += count;
        } else {
            processedCounts[key] = count;
        }
    }

    if (otherCount > 0) {
        processedCounts['Other'] = otherCount;
    }

    return processedCounts;
};