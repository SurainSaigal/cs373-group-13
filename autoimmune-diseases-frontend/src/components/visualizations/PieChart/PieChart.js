'use client'

import React, { useEffect, useRef, useState } from 'react';
import { select } from 'd3';
import { setupD3PieChart } from './D3Setup';
import { fetchAttributeCounts } from './Data';

const PieChart = () => {
    const chartRef = useRef(null);
    const [attributeCounts, setAttributeCounts] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const counts = await fetchAttributeCounts();
            setAttributeCounts(counts);
        };

        fetchData();
    }, []); 

    useEffect(() => {
        if (chartRef.current && attributeCounts) {
            const svg = select(chartRef.current);
            setupD3PieChart(svg, attributeCounts);
        }
    }, [attributeCounts]);

    return <svg ref={chartRef} />;
};

export default PieChart;