import * as d3 from "d3";
// import mapImage from '/images/Map.svg'; // Import the image explicitly

export const setupScatterPlot = (svg, data) => {
    const width = 500;
    const height = 500;

    svg.selectAll("*").remove();

    svg.append("image")
        // .attr('href', mapImage) // Use the imported image
        .attr("width", width * 1.5)
        .attr("height", height)
        .attr("x", 0)
        .attr("y", 0);

    const startX = -126;
    const tempX = 60;

    const startY = 21.2;
    const tempY = 25;

    const xScale = d3
        .scaleLinear()
        .domain([startX, startX + tempX])
        .range([0, width]);

    const yScale = d3
        .scaleLinear()
        .domain([startY, startY + tempY])
        .range([height, 100]);

    const tooltipDiv = d3
        .select("body")
        .append("div")
        .style("position", "absolute")
        .style("background", "yellow")
        .style("padding", "5px")
        .style("border-radius", "5px")
        .style("display", "none");

    svg.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", (d) => xScale(d.coordinates.lng))
        .attr("cy", (d) => yScale(d.coordinates.lat))
        .attr("r", 5)
        .attr("fill", "blue")
        .on("mouseover", (event, d) => {
            tooltipDiv
                .style("display", "block")
                .html(
                    `<strong>${d.name}</strong><br />` +
                        `${d.city}, ${d.attribute_state}<br />` +
                        `${d.address}<br />` +
                        `Lat: ${d.coordinates.lat}<br /> Lng: ${d.coordinates.lng}`
                );
        })
        .on("mousemove", (event) => {
            tooltipDiv.style("left", `${event.pageX + 10}px`).style("top", `${event.pageY + 10}px`);
        })
        .on("mouseout", () => {
            tooltipDiv.style("display", "none");
        });

    svg.append("g").attr("transform", `translate(0, ${height})`).call(d3.axisBottom(xScale));

    svg.append("g").call(d3.axisLeft(yScale));
};
