export const fetchTreatmentCenters = async () => {
    const response = await fetch('https://api.autoimmunediseasecenter.me/treatment_center');
    if (!response.ok) {
        throw new Error('Failed to fetch data');
    }
  
    return response.json();
  };  