'use client'

import { useEffect, useRef, useState } from 'react';
import { select } from 'd3';
import { setupScatterPlot } from './D3Setup';
import { fetchTreatmentCenters } from './Data';

const ScatterPlot = () => {
    const svgRef = useRef(null);
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const treatmentCenters = await fetchTreatmentCenters();
            setData(treatmentCenters);
        };

        fetchData();
    }, []);

    useEffect(() => {
        if (svgRef.current && data.length > 0) {
            const svg = select(svgRef.current);
            setupScatterPlot(svg, data);
        }
    }, [data]);

    return (
        <div>
            <svg ref={svgRef} width={800} height={600} />
        </div>
    );
};

export default ScatterPlot;