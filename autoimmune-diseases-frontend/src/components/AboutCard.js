import { FaCodeCommit } from "react-icons/fa6";
import { AiOutlineIssuesClose } from "react-icons/ai";
import { GrCodeSandbox } from "react-icons/gr";
import { ReactTyped } from "react-typed";
import { useState } from "react";

const AboutCard = ({ firstName, lastName, commits, issues, unit_tests, bio, responsibilities }) => {
    const [cursor, setCursor] = useState(false);
    return (
        <div className="card justify-content-center" style={{ width: "18rem", height: "38rem" }}>
            <img
                src={`/images/${firstName}.jpeg`}
                className="card-img-top"
                alt={firstName}
                style={{ width: "18rem" }}
            />
            <div className="card-body">
                <h5 className="card-title text-center">{firstName + " " + lastName}</h5>
                <p className="card-text">{bio}</p>
                <p className="card-text">
                    <strong>Responsibilities</strong>: {responsibilities}
                </p>

                <p style={{ marginBottom: "0.5em" }}>
                    <FaCodeCommit className="me-2" size={27} />
                    <ReactTyped
                        strings={[`<strong>${commits}</strong> commits`]}
                        typeSpeed={70}
                        showCursor={cursor}
                    />
                </p>
                <p style={{ marginBottom: "0.5em" }}>
                    <AiOutlineIssuesClose className="me-2" size={24} />
                    <ReactTyped
                        strings={[`<strong>${issues}</strong> issues closed`]}
                        typeSpeed={70}
                        showCursor={cursor}
                        startDelay={1300}
                    />
                </p>
                <p style={{ marginBottom: "0.5em" }}>
                    <GrCodeSandbox className="me-2" size={24} />
                    <ReactTyped
                        strings={[`<strong>${unit_tests}</strong> unit tests`]}
                        typeSpeed={70}
                        showCursor={cursor}
                        startDelay={2600}
                    />
                </p>
            </div>
        </div>
    );
};

export default AboutCard;
