export const getAttributes = (data_object) => {
    let attributes = {};
    for (let key in data_object) {
        if (key.startsWith("attribute_")) {
            let attribute_name = key
                .substring(10)
                .split("_")
                .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                .join(" ");
            attributes[attribute_name] = data_object[key];
        }
    }
    return attributes;
};
