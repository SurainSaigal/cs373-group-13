const ModelCard = ({ diseaseName, attributes, linkname, type, searchTerm, image }) => {
    const highlightText = (text) => {
        if (!searchTerm) {
            return text;
        }

        let words = searchTerm.split(" ").filter(Boolean);
        const regex = new RegExp(`(${words.join("|")})`, "gi");

        return text.replace(regex, "<mark>$1</mark>");
    };

    const highlightedName = highlightText(diseaseName);
    let bgrnd = "";
    let border = "";
    if (type === "diseases") {
        bgrnd = "#34d0ff";
        border = "#01c4ff";
    } else if (type === "treatments") {
        bgrnd = "#84e599";
        border = "#65df80";
    } else if (type === "treatment-centers") {
        bgrnd = "#80f3ec";
        border = "#00E7D9";
    }

    return (
        <a href={`/${type}/${linkname}`} style={{ textDecoration: "none" }}>
            <div
                className="card justify-content-center card-hover-effect"
                style={{
                    backgroundColor: bgrnd,
                    width: "18rem",
                    height: "30rem",
                    border: `3px solid ${border}`,
                }}
            >
                <img
                    className="card-img-top"
                    src={image}
                    alt={diseaseName}
                    style={{ height: "10rem", width: "auto" }}
                />
                <div className="card-body">
                    <h5
                        className="card-title text-center mb-3"
                        dangerouslySetInnerHTML={{ __html: highlightedName }}
                    />
                    {Object.keys(attributes).map((attribute, index) => (
                        <p
                            key={index}
                            className=""
                            dangerouslySetInnerHTML={{
                                __html: highlightText(
                                    `<strong>${attribute}</strong>: ${attributes[attribute]}`
                                ),
                            }}
                        />
                    ))}
                </div>
            </div>
        </a>
    );
};

export default ModelCard;
