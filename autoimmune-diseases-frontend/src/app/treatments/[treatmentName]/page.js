import { notFound } from "next/navigation";
import { getAttributes } from "@/components/GetAttributes.js";
import { backendUrl } from "@/helpers/CallBackend.js";
import LinkCard from "@/components/LinkCard.js";
export const dynamicParams = true;
export const revalidate = 100;

export default async function Page({ params }) {
    const id = params.treatmentName;

    let fetch_success = false;
    let file = null;
    while (!fetch_success) {
        file = await fetch(`${backendUrl}/treatment?id=${id}`);
        if (file.status == 404) {
            return notFound();
        }
        if (file.ok) {
            fetch_success = true;
        }
    }

    const treatment = await file.json();
    let diseases = [];
    let centers = [];
    const attributes = getAttributes(treatment);
    for (let i = 0; i < treatment.connected_diseases.length; i++) {
        let fetch_success = false;
        let data;
        while (!fetch_success) {
            data = await fetch(`${backendUrl}/disease?id=${treatment.connected_diseases[i]}`);
            if (file.ok) {
                fetch_success = true;
            }
        }
        const disease = await data.json();
        diseases.push({
            name: disease.name,
            id: disease.id,
        });
    }

    for (let i = 0; i < treatment.connected_centers.length; i++) {
        let fetch_success = false;
        let data;
        while (!fetch_success) {
            data = await fetch(
                `${backendUrl}/treatment_center?id=${treatment.connected_centers[i]}`
            );
            if (file.ok) {
                fetch_success = true;
            }
        }
        const center = await data.json();
        centers.push({
            name: center.name,
            id: center.id,
        });
    }
    return (
        <>
            <h1 className="text-center mt-1">
                <strong>{treatment.name}</strong>
            </h1>
            <ul className="ms-5 me-5 d-flex flex-wrap text-muted justify-content-between fst-italic">
                {Object.keys(attributes).map((attribute, index) => (
                    <li className="me-5" key={index}>
                        {attribute}: {attributes[attribute]}
                    </li>
                ))}
            </ul>
            <div className="row">
                <div className="col-md-6">
                    <div className="ms-4 me-3">
                        <div className="row">
                            <h2>Overview</h2>
                            <p>{treatment.overview}</p>
                            <div className="col-md-6 d-flex flex-column align-items-center">
                                <h3>Possible Side Effects:</h3>
                                <ul>
                                    {treatment.side_effects.map((effect, index) => (
                                        <li key={index}>{effect}</li>
                                    ))}
                                </ul>
                            </div>

                            <h3 className="text-center">Diseases Treated</h3>
                            <div className="row ms-1 d-flex align-items-center">
                                {diseases.map((disease, index) => (
                                    <div
                                        key={index}
                                        className="col-xs-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center align-items-center mb-3"
                                    >
                                        <LinkCard
                                            name={disease.name}
                                            type="diseases"
                                            id={disease.id}
                                        />
                                    </div>
                                ))}
                            </div>
                            <h3 className="text-center">Treatment Centers Offering</h3>
                            <div className="row ms-1 d-flex align-items-center">
                                {centers.map((center, index) => (
                                    <div
                                        key={index}
                                        className="col-xs-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center align-items-center mb-3"
                                    >
                                        <LinkCard
                                            name={center.name}
                                            type="treatment-centers"
                                            id={center.id}
                                        />
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-lg-6 d-flex flex-column align-items-center mt-4 mb-5">
                    <div className="iframe-container">
                        <iframe
                            className="responsive-iframe"
                            src={treatment.video}
                            title="YouTube video player"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                            allowFullScreen={true}
                        ></iframe>
                    </div>
                    <img
                        className="mt-5 img-fluid"
                        src={treatment.image}
                        alt={treatment.name}
                        style={{ width: "90%" }}
                    />
                </div>
            </div>
        </>
    );
}

export async function generateStaticParams() {
    const posts = await fetch(`${backendUrl}/treatment`).then((res) => res.json());
    return posts.map((treatment) => ({
        treatmentName: treatment.id.toString(),
    }));
}
