import Search from "@/components/Search.js";

const Treatments = () => {
    return (
        <div className="container">
            <h1 className="text-center mb-3 mt-3">Treatments</h1>
            <Search
                type="treatment"
                sortable={["name", "attribute_cost", "attribute_success_rate"]}
                filterable={{
                    attribute_availability: [
                        "Alternative Medicine",
                        "Digital Availability",
                        "Emergency Services",
                        "General Healthcare",
                        "Guided Self-care",
                        "Home Healthcare",
                        "Hospital Services",
                        "Monitored Healthcare",
                        "Over-the-counter",
                        "Pharmacy Services",
                        "Prescription Required",
                        "Referral Required",
                        "Rehabilitation Services",
                        "Self-managed Care",
                        "Specialized Healthcare",
                        "Specialized Research",
                        "Surgical Services",
                        "Therapy Services",
                        "Universal Access",
                        "Wellness Services",
                    ],
                    attribute_class: [
                        "Addiction Management",
                        "Cancer Therapy",
                        "Complementary Therapy",
                        "Diabetes Management",
                        "Diagnostic",
                        "Dietary Supplements",
                        "Experimental Therapy",
                        "Fungal Infection Control",
                        "General Medical Treatment",
                        "General Therapeutic",
                        "Immune Support",
                        "Immune System Management",
                        "Immune System Suppression",
                        "Infection Control",
                        "Inflammation Management",
                        "Integrated Medical and Psychological Treatment",
                        "Lifestyle Management",
                        "Medical Services",
                        "Pain Management",
                        "Preventive Medicine",
                        "Preventive and Rehabilitative Medicine",
                        "Procedural Treatment",
                        "Psychological Therapy",
                        "Psychosocial Support",
                        "Regenerative Medicine",
                        "Rehabilitation Services",
                        "Supportive Care",
                        "Targeted Therapy",
                        "Technological Interventions",
                        "Viral Infection Control",
                    ],
                    attribute_type: [
                        "Application",
                        "Care",
                        "Device",
                        "Diet",
                        "Field",
                        "Infusion",
                        "Injectable",
                        "Injectable/Infusion",
                        "Intervention",
                        "Medical Intervention",
                        "Oral",
                        "Oral/Injectable",
                        "Oral/Topical/Injectable",
                        "Practice",
                        "Procedure",
                        "Program",
                        "Program/Therapy",
                        "Protocol",
                        "Service",
                        "Strategy",
                        "Support",
                        "Systemic",
                        "Systemic and Topical",
                        "Technique",
                        "Technology",
                        "Therapy",
                        "Topical/Oral",
                        "Treatment",
                    ],
                }}
            />
        </div>
    );
};

export default Treatments;
