import Visual from "@/components/prob_visualizations/Visual.js";

const visual = () => {
    return (
        <div className="container mt-2">
            <h1 className="text-center mb-4">Provider Data Visualizations</h1>
            <Visual />
            <h1 className="text-center mb-4">Provider Critique</h1>
            <p className="text-center">
                <strong>What did they do well?</strong>
                <br />
                The Texas Trauma Support Website has a beautiful splash page, clean UI, and is easy
                to use and responsive. The data is well organized and the technical report was
                thorough and easy to understand.
            </p>

            <p className="text-center">
                <strong>How effective was their RESTful API?</strong>
                <br />
                The Texas Trauma Support API offers a range of endpoints for accessing individual
                records, or comprehensive lists for therapists, facilities, support groups, and Yelp
                reviews. Its robust filtering and sorting options based on various criteria such as
                specialization, location, and price range enhance usability. Overall this RESTful
                API is very effective in providing an endpoint to query the data that their website
                provides.
            </p>

            <p className="text-center">
                <strong>How well did they implement your user stories?</strong>
                <br />
                The team responded to our user stories with comments in a timely manner. They
                implemented most of the user stories very well, and had valid reasons not to
                implement others.
            </p>

            <p className="text-center">
                <strong>What did we learn from their website?</strong>
                <br />
                We have learned the variety in the different types of trauma. We also learned about
                information needed for finding a suitable therapist, support group, and facilities.
                Lastly, we learned how to take care of oneself when dealing with trauma and ways to
                support trauma victims.
            </p>

            <p className="text-center">
                <strong>What can they do better?</strong>
                <br />
                Some images on their model pages have extra white spaces. This makes them smaller
                and necessary and decreases the accesibility of their website.
            </p>

            <p className="text-center">
                <strong>What puzzles us about their website?</strong>
                <br />
                {`
                The "ratings" for Trauma Facilities are a little hard for us to understand given
                that there is a list of ratings from different people rather than an overall average
                value.
                `}
            </p>
        </div>
    );
};

export default visual;
