"use client";
import React, { useEffect, useState } from "react";
import AboutCard from "@/components/AboutCard.js";
import ToolCard from "@/components/ToolCard.js";
const About = () => {
    const project_id = "54520385";
    const [commitInfo, setCommitInfo] = useState({
        default: { commits: 0, issues: 0 },
    });
    const [cardData, setCardData] = useState([]);
    const gitLabNames = [
        "Surain Saigal",
        "nicolecao2002",
        "kathyxcui",
        "Callum Longenecker",
        "Jose Alvarez Loredo",
        "Enrique",
    ];

    const getGitlabData = () => {
        let dict_data = { default: { commits: 0, issues: 0 } };
        gitLabNames.forEach((username, i) => {
            dict_data[username] = {
                commits: 0,
                issues: 0,
            };
        });

        async function fetchAllCommits() {
            let page = 1;
            let hasNextPage = true;

            while (hasNextPage) {
                const response = await fetch(
                    `https://gitlab.com/api/v4/projects/${project_id}/repository/commits?all=true&per_page=100&page=${page}`
                );

                if (response.headers.get("X-Next-Page")) {
                    page++;
                } else {
                    hasNextPage = false;
                }

                const data = await response.json();
                data.forEach((commit) => {
                    if (dict_data[commit.author_name]) {
                        dict_data[commit.author_name]["commits"] += 1;
                    }
                });
            }
        }

        async function fetchAllIssues() {
            let page = 1;
            let hasNextPage = true;

            while (hasNextPage) {
                const response = await fetch(
                    `https://gitlab.com/api/v4/projects/${project_id}/issues?state=closed&scope=all&per_page=100&page=${page}`
                );

                if (response.headers.get("X-Next-Page")) {
                    page++;
                } else {
                    hasNextPage = false;
                }

                const data = await response.json();
                data.forEach((issue) => {
                    let name = issue.closed_by.name;
                    if (dict_data[name]) {
                        dict_data[name]["issues"] += 1;
                    } else {
                        if (name === "Kathy Cui") {
                            // aliasing issue
                            dict_data["kathyxcui"]["issues"] += 1;
                        }
                    }
                });
            }
        }

        Promise.all([fetchAllCommits(), fetchAllIssues()]).then(() => {
            setCommitInfo(dict_data);
        });
    };

    useEffect(() => {
        getGitlabData();
    }, []);

    useEffect(() => {
        setCardData([
            {
                firstName: "Surain",
                lastName: "Saigal",
                commits: commitInfo["Surain Saigal"]
                    ? commitInfo["Surain Saigal"].commits
                    : commitInfo["default"].commits,
                issues: commitInfo["Surain Saigal"]
                    ? commitInfo["Surain Saigal"].issues
                    : commitInfo["default"].issues,
                unit_tests: 2,
                bio: "I am a 2nd year CS student. I enjoy playing bass and riding my bike.",
                responsibilities: "Frontend",
            },
            {
                firstName: "Nicole",
                lastName: "Cao",
                commits: commitInfo["nicolecao2002"]
                    ? commitInfo["nicolecao2002"].commits
                    : commitInfo["default"].commits,
                issues: commitInfo["nicolecao2002"]
                    ? commitInfo["nicolecao2002"].issues
                    : commitInfo["default"].issues,
                unit_tests: 20,
                bio: "I am a junior studying computer science. I like drawing and painting during my free time.",
                responsibilities: "Frontend",
            },
            {
                firstName: "Kathy",
                lastName: "Cui",
                commits: commitInfo["kathyxcui"]
                    ? commitInfo["kathyxcui"].commits
                    : commitInfo["default"].commits,
                issues: commitInfo["kathyxcui"]
                    ? commitInfo["kathyxcui"].issues
                    : commitInfo["default"].issues,
                unit_tests: 4,
                bio: "I’m a CS major at UT Austin. My hobbies are playing volleyball and videography.",
                responsibilities: "Frontend",
            },
            {
                firstName: "Callum",
                lastName: "Longenecker",
                commits: commitInfo["Callum Longenecker"]
                    ? commitInfo["Callum Longenecker"].commits
                    : commitInfo["default"].commits,
                issues: commitInfo["Callum Longenecker"]
                    ? commitInfo["Callum Longenecker"].issues
                    : commitInfo["default"].issues,
                unit_tests: 5,
                bio: "Hello! I am a graduating senior at UT Austin majoring in Computer Science. I love powerlifting and producing/listening to music.",
                responsibilities: "Backend",
            },
            {
                firstName: "Jose",
                lastName: "Alvarez Loredo",
                commits:
                    commitInfo["Jose Alvarez Loredo"] && commitInfo["Enrique"]
                        ? commitInfo["Jose Alvarez Loredo"].commits + commitInfo["Enrique"].commits
                        : commitInfo["default"].commits,
                issues: commitInfo["Jose Alvarez Loredo"]
                    ? commitInfo["Jose Alvarez Loredo"].issues
                    : commitInfo["default"].issues,
                unit_tests: 0,
                bio: "I'm a CS and Math double major at UT, balancing my academic pursuits with soccer and gym workouts.",
                responsibilities: "Backend",
            },
        ]);
    }, [commitInfo]);

    const sourcesData = [
        {
            name: "Autoimmune Association",
            link: "https://autoimmune.org/disease-information/",
        },
        {
            name: "Penn Medicine",
            link: "https://www.pennmedicine.org",
        },
        {
            name: "National Institute of Environmental Health Sciences",
            link: "https://www.niehs.nih.gov/health/topics/conditions/autoimmune",
        },
        {
            name: "YouTube",
            link: "https://youtube.com",
        },
    ];

    const toolData = [
        {
            toolName: "GitLab",
            imageSource: "/images/gitlab.png",
            link: "https://gitlab.com/",
        },
        {
            toolName: "Next.js",
            imageSource: "/images/nextjs.png",
            link: "https://nextjs.org/",
        },
        {
            toolName: "React",
            imageSource: "/images/react.png",
            link: "https://reactjs.org/",
        },
        {
            toolName: "Bootstrap",
            imageSource: "/images/bootstrap.png",
            link: "https://getbootstrap.com/",
        },
        {
            toolName: "Postman",
            imageSource: "/images/postman.png",
            link: "https://www.postman.com/",
        },
        {
            toolName: "AWS",
            imageSource: "/images/aws.png",
            link: "https://aws.amazon.com/",
        },
        {
            toolName: "Docker",
            imageSource: "/images/docker.png",
            link: "https://www.docker.com/",
        },
        {
            toolName: "Jest",
            imageSource: "/images/jest.png",
            link: "https://jestjs.io/",
        },
        {
            toolName: "Selenium",
            imageSource: "/images/selenium.png",
            link: "https://www.selenium.dev/",
        },
    ];

    return (
        <div className="container mt-2">
            <h1 className="text-center mb-4">About Us</h1>
            <p className="text-center mb-4 about-text">
                The goal of this website is to educate people on various autoimmune diseases and
                their respective treatment options and centers. Here, you can find information on
                many autoimmune diseases, their symptoms, treatments used to target them, and where
                you may be able to go to receive treatment. We intend to provide a platform for use
                by both those who struggle with an autoimmune disease and those who wish to learn
                more about them.
            </p>
            <p className="text-center mb-4 about-text">
                Integrating this disparate data has shown that many autoimmune diseases share common
                treatment options and centers, and that many diseases are even treatable via more
                than one method. We hope this wesbite can give anyone struggling a complete overview
                of their options.
            </p>
            <h1 className="text-center mb-4">The Team</h1>
            <div className="row justify-content-center">
                {cardData.map((data, index) => (
                    <div
                        key={index}
                        className="col-12 col-md-6 col-lg-4 d-flex justify-content-center text-center mb-4"
                    >
                        <AboutCard {...data} />
                    </div>
                ))}
            </div>

            <h1 className="text-center mb-4">Tools Used</h1>
            <div className="row justify-content-center">
                {toolData.map((tool, index) => (
                    <div
                        key={index}
                        className="col-12 col-md-6 col-lg-4 d-flex justify-content-center text-center mb-4"
                    >
                        <ToolCard {...tool} />
                    </div>
                ))}
            </div>
            <h1 className="text-center mb-4">Data Sources</h1>
            <ul className="list-group list-group-flush mb-4">
                {sourcesData.map((source, index) => (
                    <li key={index} className="list-group-item">
                        <a href={source.link} target="_blank">
                            {source.name}
                        </a>
                    </li>
                ))}
            </ul>
            <h1 className="text-center mb-4">Project Documentation</h1>
            <ul className="list-group list-group-flush mb-4">
                <li key="1" className="list-group-item">
                    <a href="https://gitlab.com/SurainSaigal/cs373-group-13" target="_blank">
                        GitLab Repo
                    </a>
                </li>
                <li key="2" className="list-group-item">
                    <a
                        href="https://documenter.getpostman.com/view/32898971/2sA2r3Z69s"
                        target="_blank"
                    >
                        Postman Documentation
                    </a>
                </li>
            </ul>
        </div>
    );
};

export default About;
