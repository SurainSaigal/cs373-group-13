import "bootstrap/dist/css/bootstrap.min.css";
import { Inter } from "next/font/google";
import "./globals.css";
import BootstrapClient from "@/components/BootstrapClient.js";
import NavBar from "@/components/NavBar.js";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
    title: "Autoimmune Diseases",
    description: "SWE373 Spring 2024- Group 13 Project",
};

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <body className={inter.className}>
                <BootstrapClient />
                <NavBar />
                {children}
            </body>
        </html>
    );
}
