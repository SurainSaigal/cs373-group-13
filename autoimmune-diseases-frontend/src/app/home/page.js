import React from "react";
import "./page.css";
import { Container, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Cabin } from "next/font/google";

const cabin = Cabin({
    weight: "600",
    subsets: ["latin"],
});

const Home = () => {
    return (
        <Container fluid>
            <Row
                className="first-row d-flex align-items-center justify-content-center"
                style={{
                    paddingTop: "3rem",
                    position: "relative",
                    backgroundColor: "rgba(128, 128, 255, 0.5)",
                }}
            >
                <img
                    className="bg-image-container w-75 h-75"
                    src="/images/intro_image.jpg"
                    alt="Description of the image"
                    style={{
                        objectFit: "cover",
                        objectPosition: "center",
                    }}
                />
            </Row>

            <Row className="second-row" style={{ backgroundColor: "rgba(128, 128, 255, 0.5)" }}>
                <Col
                    md={12}
                    className={`text-center d-flex align-items-center justify-content-center ${cabin.className}`}
                >
                    <h1
                        style={{
                            color: "#4b1e9d",
                            fontSize: "min(max(3.5rem, 5vw), 2.5rem)",
                            fontWeight: 600,
                            marginBottom: "rem",
                            marginTop: "3rem",
                            overflow: "hidden",
                            padding: "0 3rem",
                        }}
                    >
                        Welcome to the Autoimmune Disease Resource Center
                    </h1>
                </Col>
            </Row>
            <Row
                className="second-row text-dark p-5"
                style={{ backgroundColor: "rgba(128, 128, 255, 0.5)" }}
            >
                <Col md={6}>
                    <div className="larger-text ">
                        <p>
                            Living with an autoimmune disease can be challenging, but you are not
                            alone. Whether you are navigating the complexities of diagnosis, seeking
                            information on treatment options, or looking for support from others who
                            understand, we are here to provide a comprehensive resource tailored to
                            your needs.
                        </p>
                        <p>
                            At our website, you will find a wealth of information about various
                            autoimmune diseases, including their causes, symptoms, and management
                            strategies. From rheumatoid arthritis and lupus to multiple sclerosis
                            and type 1 diabetes, we cover a wide range of conditions to empower you
                            with knowledge and support.
                        </p>
                        <p>
                            Our mission is to educate, inspire, and empower individuals affected by
                            autoimmune diseases to lead fulfilling lives. Through informative
                            articles, expert interviews, and community forums, we strive to foster a
                            sense of connection and understanding among our visitors.
                        </p>
                        <p>
                            Whether you are newly diagnosed or have been living with an autoimmune
                            condition for years, we are here to support you every step of the way.
                            Explore our resources and discover the tools and insights you need to
                            manage your health and well-being effectively.
                        </p>
                    </div>
                </Col>
                <Col md={6}>
                    <img
                        src="/images/auto.jpg"
                        alt="Different Autoimmune Disease Type"
                        className="img-fluid mb-4"
                    />
                </Col>
            </Row>

            <div className="row" style={{ backgroundColor: "rgba(128, 128, 255, 0.5)" }}>
                <div className="col-md-6 d-flex flex-column align-items-center">
                    <h3
                        style={{
                            color: "#4b1e9d",
                        }}
                    >
                        Autoimmune Disease PechaKucha{" "}
                    </h3>
                    <iframe
                        className="responsive-iframe2"
                        src="https://www.youtube.com/embed/u3RF6P8gsU8?si=49cQBimhf8QJhAkq"
                        title="Pecha Kucha"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen={true}
                    ></iframe>
                </div>

                <div className="col-md-5 d-flex flex-column ms-5 larger-text">
                    <h2
                        className={`text-center mb-4 Header-style-left-typewriter ${cabin.className}`}
                        style={{
                            color: "#4b1e9d",
                            fontSize: "min(max(3.5rem, 5vw), 1.5rem)",
                            fontWeight: 600,
                            marginBottom: "3rem",
                            marginTop: "3rem",
                            overflow: "hidden",
                            padding: "0 3rem",
                        }}
                    >
                        How To Help
                    </h2>
                    <p>
                        Here are some ways you can support a loved one with an autoimmune disease:
                    </p>
                    <ul style={{ textAlign: "left" }}>
                        <li>Educate Yourself.</li>
                        <li>Respect Their Limits.</li>
                        <li>Accommodate Their Needs.</li>
                        <li>Encourage Self-Care.</li>
                        <li>Be Patient.</li>
                        <li>Be Understanding.</li>
                        <li>Stay Positive.</li>
                    </ul>
                </div>
            </div>

            <div
                style={{
                    height: "3rem",
                    padding: 0,
                    margin: 0,
                    backgroundColor: "rgba(128, 128, 255, 0.5)",
                }}
            ></div>
        </Container>
    );
};

export default Home;
