import Search from "@/components/Search.js";

const Diseases = () => {
    return (
        <div className="container">
            <h1 className="text-center mb-3 mt-3">Diseases</h1>
            <Search
                type="disease"
                sortable={[
                    "name",
                    "attribute_year_discovered",
                    "attribute_mortality_rate",
                    "attribute_age_of_onset",
                ]}
                filterable={{
                    attribute_rarity: ["Common", "Rare", "Uncommon", "Varies", "Very Rare"],
                    attribute_type: [
                        "Autoimmune Disorders",
                        "Cardiovascular Disorders",
                        "Digestive Disorders",
                        "Endocrine Disorders",
                        "Lymphatic Disorders",
                        "Multi-system Disorders",
                        "Musculoskeletal Disorders",
                        "Neurological Disorders",
                        "Ophthalmological Disorders",
                        "Pain Disorders",
                        "Reproductive Disorders",
                        "Respiratory Disorders",
                        "Rheumatic Disorders",
                        "Skin Disorders",
                        "Vascular Disorders",
                    ],
                    leading_cause: [
                        "Autoimmune Disorders",
                        "Genetic Factors",
                        "Infections",
                        "Unknown Causes",
                    ],
                }}
            />
        </div>
    );
};

export default Diseases;
