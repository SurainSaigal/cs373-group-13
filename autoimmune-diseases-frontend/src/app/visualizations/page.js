import Visual from "@/components/visualizations/Visual.js";

const visual = () => {
    return (
        <div className="container mt-2">
            <h1 className="text-center mb-4">Our Data Visualizations</h1>
            <Visual />
            <h1 className="text-center mb-4">Self Critique</h1>
            <p className="text-center">
                <strong>What did we do well?</strong>
                <br />
                As a team we were able to effectively play to our strengths and learn what roles
                best fit each of us. Our team was well suited to adapting to adversity that we
                encountered along the way.
            </p>

            <p className="text-center">
                <strong>What did we learn?</strong>
                <br />
                {`We learned a lot of new technologies surrounding Software Engineering. We learned a
                lot about AWS and the services provided by AWS as well as how to create and interact
                with a database. We also learned about Next.js and newer web technologies like SSG,
                along with test-driven development and how to develop considering tests.`}
            </p>

            <p className="text-center">
                <strong>What did we teach each other?</strong>
                <br />
                {`
                We were able to teach each other how to be flexible and adapt to each other’s
                schedules. We were also able to teach each other about the individual aspects of the
                project as we worked on them.
                `}
            </p>

            <p className="text-center">
                <strong>What can we do better?</strong>
                <br />
                We could probably work towards having a better data scraping system. We could also
                do better with adding additional endpoints to the API for future additions to the
                functionality.
            </p>

            <p className="text-center">
                <strong>What effect did the peer reviews have?</strong>
                <br />
                We were able to adapt to any criticisms and swiftly implement any changes
                recommended.
            </p>

            <p className="text-center">
                <strong>What puzzles us?</strong>
                <br />
                {`
                We are slightly puzzled as to how to collect some of the necessary data as it wasn’t
                widely available online.`}
            </p>
        </div>
    );
};

export default visual;
