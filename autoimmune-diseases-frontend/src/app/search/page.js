"use client";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { backendUrl } from "@/helpers/CallBackend.js";
import ModelCard from "@/components/ModelCard.js";
import { getAttributes } from "@/components/GetAttributes.js";

const SearchPage = () => {
    const [diseases, setDiseases] = useState([]);
    const [treatments, setTreatments] = useState([]);
    const [centers, setCenters] = useState([]);
    const [term, setTerm] = useState("");
    const [total, setTotal] = useState(0);
    const searchParams = useSearchParams();

    useEffect(() => {
        setTerm(searchParams.get("term"));
        const search_term = searchParams.get("term").replaceAll(" ", "_");
        Promise.all([
            fetch(`${backendUrl}/v2/disease?page=1&per_page=200&search=${search_term}`)
                .then((response) => response.json())
                .then((data) => {
                    console.log("diseases", data);
                    setDiseases(data.results);
                    return data.results.length;
                }),

            fetch(`${backendUrl}/v2/treatment?page=1&per_page=200&search=${search_term}`)
                .then((response) => response.json())
                .then((data) => {
                    setTreatments(data.results);
                    return data.results.length;
                }),

            fetch(`${backendUrl}/v2/treatment_center?page=1&per_page=200&search=${search_term}`)
                .then((response) => response.json())
                .then((data) => {
                    setCenters(data.results);
                    return data.results.length;
                }),
        ]).then((lengths) => {
            const total = lengths.reduce((a, b) => a + b, 0);
            setTotal(total);
        });
    }, [searchParams]);

    return (
        <div className="container">
            <h1 className="text-center mb-3 mt-3">{`Search Results for \"${term}\"`}</h1>

            <h2 className="text-center mb-3 mt-3 text-gray">
                <em>{total} total</em>
            </h2>

            {diseases.length > 0 && (
                <div id="diseases">
                    <h2 className="ms-5 mb-3 mt-3">
                        <em>Diseases</em>
                    </h2>
                    <hr />
                    <div className="row">
                        {diseases.map((card, index) => {
                            const attributes = getAttributes(card);
                            return (
                                <div
                                    className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center text-center mb-4"
                                    key={index}
                                >
                                    <ModelCard
                                        key={index}
                                        diseaseName={card.name}
                                        attributes={attributes}
                                        linkname={card.id}
                                        type={`diseases`}
                                        searchTerm={term}
                                        image={card.image}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}
            {treatments.length > 0 && (
                <div id="treatments" style={{ paddingTop: "70px" }}>
                    <h2 className="ms-5 mb-3 mt-3">
                        <em>Treatments</em>
                    </h2>
                    <hr />
                    <div className="row">
                        {treatments.map((card, index) => {
                            const attributes = getAttributes(card);
                            return (
                                <div
                                    className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center text-center mb-4"
                                    key={index}
                                >
                                    <ModelCard
                                        key={index}
                                        diseaseName={card.name}
                                        attributes={attributes}
                                        linkname={card.id}
                                        type={`treatments`}
                                        searchTerm={term}
                                        image={card.image}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}
            {centers.length > 0 && (
                <div id="centers" style={{ paddingTop: "70px" }}>
                    <h2 className="ms-5 mb-3 mt-3">
                        <em>Treatment Centers</em>
                    </h2>
                    <hr />
                    <div className="row">
                        {centers.map((card, index) => {
                            const attributes = getAttributes(card);
                            return (
                                <div
                                    className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 d-flex justify-content-center text-center mb-4"
                                    key={index}
                                >
                                    <ModelCard
                                        key={index}
                                        diseaseName={card.name}
                                        attributes={attributes}
                                        linkname={card.id}
                                        type={`treatment-centers`}
                                        searchTerm={term}
                                        image={`https://maps.googleapis.com/maps/api/staticmap?zoom=14&size=400x250&key=AIzaSyB45gRh6nVLgyLb0Zbq711-XmI3440pcn0&maptype=terrain&markers=color:0xFFFF00%7C${card.coordinates.lat},${card.coordinates.lng}`}
                                    />
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}

            <nav aria-label="Pagination" className="fixed-bottom mb-3">
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <span className="page-link">Scroll to:</span>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#diseases">
                            Diseases
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#treatments">
                            Treatments
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#centers">
                            Centers
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    );
};
export default SearchPage;
