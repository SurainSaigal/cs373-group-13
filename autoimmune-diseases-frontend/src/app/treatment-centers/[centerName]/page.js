import { notFound } from "next/navigation";
import LinkCard from "@/components/LinkCard.js";
import CenterMap from "@/components/CenterMap.js";
import { backendUrl } from "@/helpers/CallBackend.js";
import { getAttributes } from "@/components/GetAttributes.js";
import { FaExternalLinkAlt } from "react-icons/fa";
export const dynamicParams = true;
export const revalidate = 100;

export default async function Page({ params }) {
    const id = params.centerName;

    let fetch_success = false;
    let file = null;
    while (!fetch_success) {
        file = await fetch(`${backendUrl}/treatment_center?id=${id}`);
        if (file.status == 404) {
            return notFound();
        }
        if (file.ok) {
            fetch_success = true;
        }
    }

    const center = await file.json();
    let attributes = getAttributes(center);
    attributes["City"] = center.city;
    let diseases = [];
    let treatments = [];

    for (let i = 0; i < center.connected_diseases.length; i++) {
        let fetch_success = false;
        let data;
        while (!fetch_success) {
            data = await fetch(`${backendUrl}/disease?id=${center.connected_diseases[i]}`);
            if (file.ok) {
                fetch_success = true;
            }
        }
        const disease = await data.json();
        diseases.push({
            name: disease.name,
            id: disease.id,
        });
    }

    for (let i = 0; i < center.connected_treatments.length; i++) {
        let fetch_success = false;
        let data;
        while (!fetch_success) {
            data = await fetch(`${backendUrl}/treatment?id=${center.connected_treatments[i]}`);
            if (file.ok) {
                fetch_success = true;
            }
        }
        const treatment = await data.json();
        treatments.push({
            name: treatment.name,
            id: treatment.id,
        });
    }

    return (
        <>
            <h1 className="text-center mt-1">
                <strong>{center.name}</strong>
            </h1>
            <ul className="ms-5 me-5 d-flex flex-wrap text-muted justify-content-between fst-italic">
                {Object.keys(attributes).map((attribute, index) => (
                    <li className="me-5" key={index}>
                        {attribute}: {attributes[attribute]}
                    </li>
                ))}
            </ul>
            <div className="row">
                <div className="col-md-6">
                    <div className="ms-4 me-3">
                        <h2>Overview</h2>
                        <p>{center.description}</p>
                        <p>
                            <strong className="">Address</strong>: {center.address}
                            <strong className="ms-3">Contact</strong>: {center.contact}
                        </p>
                        <p>
                            <a href={center.website} target="_blank" rel="noreferrer">
                                <strong>Website</strong>
                                <FaExternalLinkAlt className="ms-2" color="blue" />
                            </a>
                        </p>
                        <div className="row">
                            <h3 className="text-center">Diseases Treated</h3>
                            <div className="row ms-1 d-flex align-items-center">
                                {diseases.map((disease, index) => (
                                    <div
                                        key={index}
                                        className="col-xs-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center align-items-center mb-3"
                                    >
                                        <LinkCard
                                            name={disease.name}
                                            type="diseases"
                                            id={disease.id}
                                        />
                                    </div>
                                ))}
                            </div>
                            <h3 className="text-center">Treatments Given</h3>
                            <div className="row ms-1 d-flex align-items-center">
                                {treatments.map((treatment, index) => (
                                    <div
                                        key={index}
                                        className="col-xs-12 col-md-6 col-lg-4 d-flex flex-column justify-content-center align-items-center mb-3"
                                    >
                                        <LinkCard
                                            name={treatment.name}
                                            type="treatments"
                                            id={treatment.id}
                                        />
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 d-flex flex-column align-items-center mt-4 mb-5">
                    <div className="map-container">
                        <CenterMap lat={center.coordinates.lat} lng={center.coordinates.lng} />
                    </div>
                    <img
                        className="mt-5 img-fluid"
                        src={center.image}
                        alt={center.name}
                        style={{ width: "90%" }}
                    />
                </div>
            </div>
        </>
    );
}

export async function generateStaticParams() {
    const posts = await fetch(`${backendUrl}/treatment_center`).then((res) => res.json());
    return posts.map((center) => ({
        centerName: center.id.toString(),
    }));
}
