import Search from "@/components/Search.js";

const Centers = () => {
    return (
        <div className="container">
            <h1 className="text-center mb-3 mt-3">Treatment Centers</h1>
            <Search
                type="treatment_center"
                sortable={["name", "attribute_capacity", "attribute_state", "attribute_branch"]}
                filterable={{ attribute_class: ["Center", "Hospital", "Office"] }}
            />
        </div>
    );
};

export default Centers;
