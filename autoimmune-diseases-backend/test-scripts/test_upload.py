import requests
import json
import os

# api_url = 'https://api.autoimmunediseasecenter.me'

api_url = 'http://127.0.0.1:5000'

diseases = [
    {
        "name": "Test Disease 1",
        "attribute_type": "Endocrine",
        "attribute_rarity": "Rare",
        "attribute_year_discovered": "1855",
        "attribute_mortality_rate": "Varies",
        "attribute_age_of_onset": "30-50",
        "leading_cause": "Autoimmune destruction of the adrenal cortex",
        "overview": "Addison's Disease is a medical condition characterized by a series of symptoms and health impacts. Further details should be consulted with healthcare professionals.",
        "symptoms": [
            "sore throat",
            "fever",
            "cough"
        ],
        "video": "https://www.youtube.com/embed/wqJUifWOGEo",
        "image": "https://www.verywellhealth.com/thmb/K8CBulRblYbwEYZL0V44tqDqqsg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/addisons-disease-symptoms-cause-diagnosis-treatment-4172782_FINAL-5c4550a546e0fb0001420c10.png",
        "connected_treatments": [
            "Gene Editing Techniques",
            "Monoclonal Antibodies"
        ],
        "connected_centers": [
            "Cape Fear Arthritis Care",
            "Indigo Integrative Health Clinic"
        ]
    },
    {
        "name": "Test Disease 2",
        "attribute_type": "TEST",
        "attribute_rarity": "TEST",
        "attribute_year_discovered": "1492",
        "attribute_mortality_rate": "TEST",
        "attribute_age_of_onset": "TEST",
        "leading_cause": "TEST",
        "overview": "TEST",
        "symptoms": [
            "symptom1",
            "symptom2",
            "symptom3"
        ],
        "video": "https://www.youtube.com/embed/wqJUifWOGEo",
        "image": "https://www.verywellhealth.com/thmb/K8CBulRblYbwEYZL0V44tqDqqsg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/addisons-disease-symptoms-cause-diagnosis-treatment-4172782_FINAL-5c4550a546e0fb0001420c10.png",
        "connected_treatments": [],
        "connected_centers": []
    }
]
# URL to the API
url = api_url+'/disease'

if diseases:
    # Loop through the diseases and upload them to the API
    for disease in diseases:
        response = requests.post(url, json=disease)
        print(response.json())