import requests
import json
import os

# api_url = 'https://api.autoimmunediseasecenter.me'

api_url = 'http://127.0.0.1:5000'

response = requests.get(api_url+'/v2/disease?filter_on_rarity=Common&filter_on_rarity=Rare')
print(response.json())