from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from flask_marshmallow import Marshmallow
from flask_cors import CORS
import os


# Init app
app = Flask(__name__)
CORS(app)
basedir = os.path.abspath(os.path.dirname(__file__))

# Database
user = os.environ.get('DB_USER')
password = os.environ.get('DB_PASS')
endpoint = os.environ.get('DB_ENDPOINT')
port = os.environ.get('DB_PORT')
database_name = os.environ.get('DB_NAME')


app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{user}:{password}@{endpoint}:{port}/{database_name}'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Init ma
ma = Marshmallow(app)


# Associations
disease_treatment = db.Table('disease_treatment',
                             db.Column('disease_id', db.Integer,
                                       db.ForeignKey('disease.id')),
                             db.Column('treatment_id', db.Integer,
                                       db.ForeignKey('treatment.id'))
                             )

disease_treatment_center = db.Table('disease_treatment_center',
                                    db.Column('disease_id', db.Integer,
                                              db.ForeignKey('disease.id')),
                                    db.Column('treatment_center_id', db.Integer, db.ForeignKey(
                                        'treatment_center.id'))
                                    )

treatment_treatment_center = db.Table('treatment_treatment_center',
                                      db.Column('treatment_id', db.Integer,
                                                db.ForeignKey('treatment.id')),
                                      db.Column('treatment_center_id', db.Integer, db.ForeignKey(
                                          'treatment_center.id'))
                                      )




class Symptom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    symptom = db.Column(db.String(200))
    disease_id = db.Column(db.Integer, db.ForeignKey('disease.id'))

# Disease Class/Model
class Disease(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    attribute_type = db.Column(db.String(100))
    attribute_rarity = db.Column(db.String(100))
    attribute_year_discovered = db.Column(db.Integer)
    attribute_mortality_rate = db.Column(db.Float)
    attribute_age_of_onset = db.Column(db.Integer)
    leading_cause = db.Column(db.String(500))
    overview = db.Column(db.String(500))
    symptoms = db.relationship(
        'Symptom', backref='disease', cascade="all, delete-orphan")
    video = db.Column(db.String(300))
    image = db.Column(db.String(300))
    connected_treatments = db.relationship(
        'Treatment', secondary=disease_treatment, backref=db.backref('connected_diseases'))
    connected_centers = db.relationship(
        'TreatmentCenter', secondary=disease_treatment_center, backref=db.backref('connected_diseases'))

    def __init__(self, name, attribute_type, attribute_rarity, attribute_year_discovered, attribute_mortality_rate, attribute_age_of_onset, leading_cause, overview, video, image):
        self.name = name
        self.attribute_type = attribute_type
        self.attribute_rarity = attribute_rarity
        self.attribute_year_discovered = attribute_year_discovered
        self.attribute_mortality_rate = attribute_mortality_rate
        self.attribute_age_of_onset = attribute_age_of_onset
        self.leading_cause = leading_cause
        self.overview = overview
        self.video = video
        self.image = image


class Coordinates(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    center_id = db.Column(db.Integer, db.ForeignKey('treatment_center.id'))

    def __init__(self, lat, lng, center_id):
        self.lat = lat
        self.lng = lng
        self.center_id = center_id

# Treatement Center Class/Model
class TreatmentCenter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True)
    attribute_state = db.Column(db.String(100))
    attribute_capacity = db.Column(db.Integer)
    attribute_class = db.Column(db.String(100))
    attribute_branch = db.Column(db.String(100))
    website = db.Column(db.String(300))
    city = db.Column(db.String(100))
    address = db.Column(db.String(300))
    image = db.Column(db.String(300))
    coordinates = db.relationship(
        'Coordinates', backref='treatment_center', uselist=False, cascade="all, delete-orphan")
    contact = db.Column(db.String(100))
    description = db.Column(db.String(500))

    def __init__(self, name, attribute_state, attribute_capacity, attribute_class, attribute_branch, website, city, address, image, contact, description):
        self.name = name
        self.attribute_state = attribute_state
        self.attribute_capacity = attribute_capacity
        self.attribute_class = attribute_class
        self.attribute_branch = attribute_branch
        self.website = website
        self.city = city
        self.address = address
        self.image = image
        self.contact = contact
        self.description = description


# Side effect Class/Model
class SideEffect(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    side_effect = db.Column(db.String(200))
    treatment_id = db.Column(db.Integer, db.ForeignKey('treatment.id'))

# Treatment Class/Model
class Treatment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    attribute_class = db.Column(db.String(100))
    attribute_type = db.Column(db.String(100))
    attribute_cost = db.Column(db.Integer)
    attribute_availability = db.Column(db.String(100))
    attribute_success_rate = db.Column(db.Integer)
    image = db.Column(db.String(300))
    video = db.Column(db.String(300))
    overview = db.Column(db.String(500))
    side_effects = db.relationship(
        'SideEffect', backref='treatment', cascade="all, delete-orphan")
    connected_centers = db.relationship(
        'TreatmentCenter', secondary=treatment_treatment_center, backref=db.backref('connected_treatments'))

    def __init__(self, name, attribute_class, attribute_type, attribute_cost, attribute_availability, attribute_success_rate, image, video, overview):
        self.name = name
        self.attribute_class = attribute_class
        self.attribute_type = attribute_type
        self.attribute_cost = attribute_cost
        self.attribute_availability = attribute_availability
        self.attribute_success_rate = attribute_success_rate
        self.image = image
        self.video = video
        self.overview = overview

# Symptom Schema
class SymptomSchema(ma.Schema):
    class Meta:
        fields = ('id', 'symptom', 'disease_id')


# Disease Schema
class DiseaseSchema(ma.Schema):
    symptoms = ma.Pluck('SymptomSchema', 'symptom', many=True)
    connected_treatments = ma.Pluck('TreatmentSchema', 'id', many=True)
    connected_centers = ma.Pluck('TreatmentCenterSchema', 'id', many=True)

    class Meta:
        fields = ('id', 'name', 'attribute_type', 'attribute_rarity',
                  'attribute_year_discovered', 'attribute_mortality_rate', 'attribute_age_of_onset', 'leading_cause', 'overview', 'symptoms', 'video', 'image', 'connected_treatments', 'connected_centers')

# Coordinates Schema
class CoordinatesSchema(ma.Schema):
    class Meta:
        fields = ('id', 'lat', 'lng', 'center_id')

# Treatment Center Schema
class TreatmentCenterSchema(ma.Schema):
    coordinates = ma.Nested('CoordinatesSchema')
    connected_treatments = ma.Pluck('TreatmentSchema', 'id', many=True)
    connected_diseases = ma.Pluck('DiseaseSchema', 'id', many=True)

    class Meta:
        fields = ('id', 'name', 'attribute_state', 'attribute_capacity',
                  'attribute_class', 'attribute_branch', 'website', 'city', 'address', 'image', 'coordinates', 'contact', 'description', 'connected_diseases', 'connected_treatments')

# Side Effect Schema
class SideEffectSchema(ma.Schema):
    class Meta:
        fields = ('id', 'side_effect', 'treatment_id')

# Treatment Schema
class TreatmentSchema(ma.Schema):
    side_effects = ma.Pluck('SideEffectSchema', 'side_effect', many=True)
    connected_centers = ma.Pluck('TreatmentCenterSchema', 'id', many=True)
    connected_diseases = ma.Pluck('DiseaseSchema', 'id', many=True)

    class Meta:
        fields = ('id', 'name', 'attribute_class', 'attribute_type',
                  'attribute_cost', 'attribute_availability', 'attribute_success_rate', 'image', 'video', 'overview', 'side_effects', 'connected_diseases', 'connected_centers')


# Init schema
disease_schema = DiseaseSchema()
diseases_schema = DiseaseSchema(many=True)
treatment_center_schema = TreatmentCenterSchema()
treatment_centers_schema = TreatmentCenterSchema(many=True)
treatment_schema = TreatmentSchema()
treatments_schema = TreatmentSchema(many=True)


# POST REQUESTS

# Create a Disease in the database
@app.route('/disease', methods=['POST'])
def add_disease():
    try:
        name = request.json['name']
        attribute_type = request.json['attribute_type']
        attribute_rarity = request.json['attribute_rarity']
        attribute_year_discovered = request.json['attribute_year_discovered']
        attribute_mortality_rate = request.json['attribute_mortality_rate']
        attribute_age_of_onset = request.json['attribute_age_of_onset']
        leading_cause = request.json['leading_cause']
        overview = request.json['overview']
        video = request.json['video']
        image = request.json['image']
        symptoms = request.json['symptoms']
        connected_treatments = request.json['connected_treatments']
        connected_centers = request.json['connected_centers']

        new_disease = Disease(name=name, attribute_type=attribute_type, attribute_rarity=attribute_rarity, attribute_year_discovered=attribute_year_discovered,
                              attribute_mortality_rate=attribute_mortality_rate, attribute_age_of_onset=attribute_age_of_onset, leading_cause=leading_cause, overview=overview, video=video, image=image)
        db.session.add(new_disease)
        db.session.commit()

        for symptom in symptoms:
            new_symptom = Symptom(symptom=symptom, disease_id=new_disease.id)
            db.session.add(new_symptom)
            db.session.commit()

        for treatment in connected_treatments:
            treatment_data = Treatment.query.filter_by(name=treatment).first()
            if treatment_data:
                new_disease.connected_treatments.append(treatment_data)
            db.session.commit()

        for center in connected_centers:
            center_data = TreatmentCenter.query.filter_by(name=center).first()
            if center_data:
                new_disease.connected_centers.append(center_data)
            db.session.commit()

        return disease_schema.jsonify(new_disease)
    except Exception as e:
        return jsonify({'message': str(e)}), 500


# Create a Treatment Center in the database
@app.route('/treatment_center', methods=['POST'])
def add_treatment_center():
    try:
        name = request.json['name']
        attribute_state = request.json['attribute_state']
        attribute_capacity = request.json['attribute_capacity']
        attribute_class = request.json['attribute_class']
        attribute_branch = request.json['attribute_branch']
        website = request.json['website']
        city = request.json['city']
        address = request.json['address']
        image = request.json['image']
        contact = request.json['contact']
        description = request.json['description']
        coordinates = request.json['coordinates']
        connected_treatments = request.json['connected_treatments']
        connected_diseases = request.json['connected_diseases']

        new_treatment_center = TreatmentCenter(name=name, attribute_state=attribute_state, attribute_capacity=attribute_capacity, attribute_class=attribute_class,
                                               attribute_branch=attribute_branch, website=website, city=city, address=address, image=image, contact=contact, description=description)
        db.session.add(new_treatment_center)
        db.session.commit()

        new_coordinates = Coordinates(
            lat=coordinates['lat'], lng=coordinates['lng'], center_id=new_treatment_center.id)
        db.session.add(new_coordinates)
        db.session.commit()

        for treatment in connected_treatments:
            treatment_data = Treatment.query.filter_by(name=treatment).first()
            if treatment_data:
                new_treatment_center.connected_treatments.append(
                    treatment_data)
            db.session.commit()

        for disease in connected_diseases:
            disease_data = Disease.query.filter_by(name=disease).first()
            if disease_data:
                new_treatment_center.connected_diseases.append(disease_data)
            db.session.commit()

        return treatment_center_schema.jsonify(new_treatment_center)
    except Exception as e:
        return jsonify({'message': str(e)}), 500

# Create a Treatment in the database
@app.route('/treatment', methods=['POST'])
def add_treatment():
    try:
        name = request.json['name']
        attribute_class = request.json['attribute_class']
        attribute_type = request.json['attribute_type']
        attribute_cost = request.json['attribute_cost']
        attribute_availability = request.json['attribute_availability']
        attribute_success_rate = request.json['attribute_success_rate']
        image = request.json['image']
        video = request.json['video']
        overview = request.json['overview']
        side_effects = request.json['side_effects']
        connected_diseases = request.json['connected_diseases']
        connected_centers = request.json['connected_centers']

        new_treatment = Treatment(name=name, attribute_class=attribute_class, attribute_type=attribute_type,
                                  attribute_cost=attribute_cost, attribute_availability=attribute_availability, attribute_success_rate=attribute_success_rate, image=image, video=video, overview=overview)
        db.session.add(new_treatment)
        db.session.commit()

        for side_effect in side_effects:
            new_side_effect = SideEffect(
                side_effect=side_effect, treatment_id=new_treatment.id)
            db.session.add(new_side_effect)
            db.session.commit()

        for disease in connected_diseases:
            disease_data = Disease.query.filter_by(name=disease).first()
            if disease_data:
                new_treatment.connected_diseases.append(disease_data)
            db.session.commit()

        for center in connected_centers:
            center_data = TreatmentCenter.query.filter_by(name=center).first()
            if center_data:
                new_treatment.connected_centers.append(center_data)
            db.session.commit()

        return treatment_schema.jsonify(new_treatment)

    except Exception as e:
        return jsonify({'message': str(e)}), 500

# Get all or single Disease
@app.route('/disease', methods=['GET'])
def get_diseases():
    if request.args.get('id'):
        id = request.args.get('id')
        disease = Disease.query.get(id)
        if not disease:
            return jsonify({'message': 'Disease not found'}), 404
        return disease_schema.jsonify(disease)
    all_diseases = Disease.query.all()
    result = diseases_schema.dump(all_diseases)
    return jsonify(result)

# v2 Get all or single Disease
@app.route("/v2/disease", methods=["GET"])
def get_diseases_new():
    if request.args.get('id'):
        id = request.args.get('id')
        disease = Disease.query.get(id)
        if not disease:
            return jsonify({'message': 'Disease not found'}), 404
        return disease_schema.jsonify(disease)
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    search = request.args.get("search", default="", type=str)
    sort_order = request.args.get("sort_order", default="asc", type=str)
    sort_on = request.args.get("sort_on", default="name", type=str)
    filter_on_rarity = request.args.get(
        "filter_on_rarity", default="", type=str)
    filter_on_rarity = request.args.getlist('filter_on_rarity')
    filter_on_type = request.args.getlist('filter_on_type')
    filter_on_leading_cause = request.args.get("filter_on_leading_cause", default="", type=str)

    # initial query object
    disease_query = Disease.query
    query_terms = []

    if search:
        query_terms = search.split("_")
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            possible_year_filter = (
                Disease.attribute_year_discovered == term,) if term.isdigit() else ()
            possible_mortality_rate_filter = (
                Disease.attribute_mortality_rate == term,) if term.isdigit() else ()
            possible_age_of_onset_filter = (
                Disease.attribute_age_of_onset == term,) if term.isdigit() else ()

            ilike_filters.extend(
                [
                    Disease.name.ilike(f"%{term}%"),
                    Disease.attribute_type.ilike(f"%{term}%"),
                    Disease.attribute_rarity.ilike(f"%{term}%"),
                    *possible_year_filter,
                    *possible_mortality_rate_filter,
                    *possible_age_of_onset_filter,
                    Disease.leading_cause.ilike(f"%{term}%"),
                    Disease.overview.ilike(f"%{term}%"),
                    Symptom.symptom.ilike(f"%{term}%")
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)

        # Add a join to the Symptom model
        disease_query = disease_query.join(Disease.symptoms)

        # Apply the filter to the query
        disease_query = disease_query.filter(query_filter)
    if len(filter_on_rarity) > 0:
        # split on '_' and rejoin with ' ' to match the format in the database
        filter_on_rarity = [" ".join(rarity.split("_")) for rarity in filter_on_rarity]
        disease_query = disease_query.filter(
            Disease.attribute_rarity.in_(filter_on_rarity))
    if len(filter_on_type) > 0:
        filter_on_type = [" ".join(type.split("_")) for type in filter_on_type]
        disease_query = disease_query.filter(
            Disease.attribute_type.in_(filter_on_type))
    if len(filter_on_leading_cause) > 0:
        filter_on_leading_cause = [" ".join(leading_cause.split("_")) for leading_cause in filter_on_leading_cause]
        disease_query = disease_query.filter(
            Disease.leading_cause.in_(filter_on_leading_cause))

    # Retrieve the results without sorting for now
    disease_results = disease_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, disease_schema.dump(result)))
        for result in disease_results
    ]
    # # Sort results by relevance score and property name
    if sort_order == "asc":
        if search:
            sorted_results = sorted(relevance_scores, key=lambda x: x[1])
        else:
            # sort based on sort_on
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on))
    else:
        if search:
            sorted_results = sorted(
                relevance_scores, key=lambda x: x[1], reverse=True)
        else:
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on), reverse=True)

    # Extract the sorted housing items
    sorted_diseases = [result for result, _ in sorted_results]

    # Paginate the sorted results
    total_diseases = len(sorted_diseases)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_diseases = sorted_diseases[start_idx:end_idx]

    # Serialize the paginated housing
    result = diseases_schema.dump(paginated_diseases)

    return jsonify(
        {
            "results": result,
            "page": page,
            "per_page": per_page,
            "total_diseases": total_diseases,
        }
    )


def calculate_relevance(query_terms, result_dict):
    # Calculate the relevance score based on the number of matched terms (lower = better)
    relevance_score = 0

    # Convert all model attribute values to a single string for searching
    result_text = "".join(str(value) for value in result_dict.values()).lower()

    # check if the full query term is in the result text
    if " ".join(query_terms).lower() in result_text:
        relevance_score -= 1000

    for term in query_terms:
        if term in result_text:
            # Increase relevance score for each matched term
            relevance_score -= 1

    return relevance_score


# Get all or single Treatment Center
@app.route('/treatment_center', methods=['GET'])
def get_treatment_centers():
    if request.args.get('id'):
        id = request.args.get('id')
        treatment_center = TreatmentCenter.query.get(id)
        if not treatment_center:
            return jsonify({'message': 'Treatment Center not found'}), 404
        return treatment_center_schema.jsonify(treatment_center)
    all_treatment_centers = TreatmentCenter.query.all()
    result = treatment_centers_schema.dump(all_treatment_centers)
    return jsonify(result)

# v2 Get all or single Treatment Center
@app.route("/v2/treatment_center", methods=["GET"])
def get_treatment_centers_new():
    if request.args.get('id'):
        id = request.args.get('id')
        treatment_center = TreatmentCenter.query.get(id)
        if not treatment_center:
            return jsonify({'message': 'Treatment Center not found'}), 404
        return treatment_center_schema.jsonify(treatment_center)
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    search = request.args.get("search", default=None, type=str)
    sort_order = request.args.get("sort_order", default="asc", type=str)
    sort_on = request.args.get("sort_on", default="name", type=str)
    filter_on_class = request.args.getlist('filter_on_class')

    # initial query object
    treatment_center_query = TreatmentCenter.query

    query_terms = []

    if search:
        # log the search term
        print(f"Search term: {search}")
        # Search for the string in multiple fields
        query_terms = search.split("_")

        # Initialize an empty list to store individual ilike filters
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            possible_capacity_filter = (
                TreatmentCenter.attribute_capacity == term,) if term.isdigit() else ()
            possible_coordinates_lat_filter = (
                Coordinates.lat == term,) if term.isdigit() else ()
            possible_coordinates_lng_filter = (
                Coordinates.lng == term,) if term.isdigit() else ()

            ilike_filters.extend(
                [
                    TreatmentCenter.name.ilike(f"%{term}%"),
                    TreatmentCenter.attribute_state.ilike(f"%{term}%"),
                    *possible_capacity_filter,
                    TreatmentCenter.attribute_class.ilike(f"%{term}%"),
                    TreatmentCenter.attribute_branch.ilike(f"%{term}%"),
                    TreatmentCenter.website.ilike(f"%{term}%"),
                    TreatmentCenter.city.ilike(f"%{term}%"),
                    TreatmentCenter.address.ilike(f"%{term}%"),
                    TreatmentCenter.contact.ilike(f"%{term}%"),
                    TreatmentCenter.description.ilike(f"%{term}%"),
                    *possible_coordinates_lat_filter,
                    *possible_coordinates_lng_filter
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)

        # Add a join to the Symptom model
        treatment_center_query = treatment_center_query.join(
            TreatmentCenter.coordinates)

        # Apply the filter to the query
        treatment_center_query = treatment_center_query.filter(query_filter)

    if len(filter_on_class) > 0:
        filter_on_class = [" ".join(cls.split("_")) for cls in filter_on_class]
        treatment_center_query = treatment_center_query.filter(
            TreatmentCenter.attribute_class.in_(filter_on_class))

    # Retrieve the results without sorting for now
    treatment_center_results = treatment_center_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, treatment_center_schema.dump(result)))
        for result in treatment_center_results
    ]

    # # Sort results by relevance score and property name
    if sort_order == "asc":
        if search:
            sorted_results = sorted(relevance_scores, key=lambda x: x[1])
        else:
            # sort based on sort_on
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on))
    else:
        if search:
            sorted_results = sorted(
                relevance_scores, key=lambda x: x[1], reverse=True)
        else:
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on), reverse=True)

    # Extract the sorted items
    sorted_treatment_centers = [result for result, _ in sorted_results]

    # Paginate the sorted results
    total_treatment_centers = len(sorted_treatment_centers)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_treatment_centers = sorted_treatment_centers[start_idx:end_idx]

    # Serialize the paginated housing
    result = treatment_centers_schema.dump(paginated_treatment_centers)

    return jsonify(
        {
            "results": result,
            "page": page,
            "per_page": per_page,
            "total_treatment_centers": total_treatment_centers,
        }
    )


# Get all or single Treatment
@app.route('/treatment', methods=['GET'])
def get_treatments():
    if request.args.get('id'):
        id = request.args.get('id')
        treatment = Treatment.query.get(id)
        if not treatment:
            return jsonify({'message': 'Treatment not found'}), 404
        return treatment_schema.jsonify(treatment)
    all_treatments = Treatment.query.all()
    result = treatments_schema.dump(all_treatments)
    return jsonify(result)

# v2 Get all or single Disease
@app.route("/v2/treatment", methods=["GET"])
def get_treatments_new():
    if request.args.get('id'):
        id = request.args.get('id')
        treatment = Treatment.query.get(id)
        if not treatment:
            return jsonify({'message': 'Treatment not found'}), 404
        return treatment_schema.jsonify(treatment)
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    search = request.args.get("search", default=None, type=str)
    sort_order = request.args.get("sort_order", default="asc", type=str)
    sort_on = request.args.get("sort_on", default="name", type=str)
    filter_on_availability = request.args.getlist('filter_on_availability')
    filter_on_class = request.args.getlist('filter_on_class')
    filter_on_type = request.args.getlist('filter_on_type')

    # initial query object
    treatment_query = Treatment.query

    query_terms = []

    if search:
        # log the search term
        print(f"Search term: {search}")
        # Search for the string in multiple fields
        query_terms = search.split("_")

        # Initialize an empty list to store individual ilike filters
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            possible_cost_filter = (
                Treatment.attribute_cost == term,) if term.isdigit() else ()
            possible_success_rate_filter = (
                Treatment.attribute_success_rate == term,) if term.isdigit() else ()

            ilike_filters.extend(
                [
                    Treatment.name.ilike(f"%{term}%"),
                    Treatment.attribute_class.ilike(f"%{term}%"),
                    Treatment.attribute_type.ilike(f"%{term}%"),
                    *possible_cost_filter,
                    Treatment.attribute_availability.ilike(f"%{term}%"),
                    *possible_success_rate_filter,
                    Treatment.overview.ilike(f"%{term}%"),
                    SideEffect.side_effect.ilike(f"%{term}%")
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)

        # Add a join to the Symptom model
        treatment_query = treatment_query.join(Treatment.side_effects)

        # Apply the filter to the query
        treatment_query = treatment_query.filter(query_filter)


    if len(filter_on_availability) > 0:
        filter_on_availability = [" ".join(availability.split("_")) for availability in filter_on_availability]
        treatment_query = treatment_query.filter(
            Treatment.attribute_availability.in_(filter_on_availability))
    if len(filter_on_class) > 0:
        filter_on_class = [" ".join(cls.split("_")) for cls in filter_on_class]
        treatment_query = treatment_query.filter(
            Treatment.attribute_class.in_(filter_on_class))
    if len(filter_on_type) > 0:
        filter_on_type = [" ".join(type.split("_")) for type in filter_on_type]
        treatment_query = treatment_query.filter(
            Treatment.attribute_type.in_(filter_on_type))

    # Retrieve the results without sorting for now
    treatment_results = treatment_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, treatment_schema.dump(result)))
        for result in treatment_results
    ]

    # # Sort results by relevance score and property name
    if sort_order == "asc":
        if search:
            sorted_results = sorted(relevance_scores, key=lambda x: x[1])
        else:
            # sort based on sort_on
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on))
    else:
        if search:
            sorted_results = sorted(
                relevance_scores, key=lambda x: x[1], reverse=True)
        else:
            sorted_results = sorted(
                relevance_scores, key=lambda x: getattr(x[0], sort_on), reverse=True)

    # Extract the sorted items
    sorted_treatments = [result for result, _ in sorted_results]

    # Paginate the sorted results
    total_treatments = len(sorted_treatments)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_treatments = sorted_treatments[start_idx:end_idx]

    # Serialize the paginated housing
    result = treatments_schema.dump(paginated_treatments)

    return jsonify(
        {
            "results": result,
            "page": page,
            "per_page": per_page,
            "total_treatments": total_treatments,
        }
    )

# Update a Disease
@app.route('/disease', methods=['PUT'])
def update_disease():
    id = request.args.get('id')
    if not id:
        return jsonify({'message': 'No disease id provided'}), 400

    disease = Disease.query.get(id)
    if not disease:
        return jsonify({'message': 'Disease not found'}), 404

    disease.name = request.json['name']
    disease.attribute_type = request.json['attribute_type']
    disease.attribute_rarity = request.json['attribute_rarity']
    disease.attribute_year_discovered = request.json['attribute_year_discovered']
    disease.attribute_mortality_rate = request.json['attribute_mortality_rate']
    disease.attribute_age_of_onset = request.json['attribute_age_of_onset']
    disease.leading_cause = request.json['leading_cause']
    disease.overview = request.json['overview']
    disease.video = request.json['video']
    disease.image = request.json['image']

    db.session.commit()

    # NOTE: no update to symptoms, connected_treatments, or connected_centers

    return disease_schema.jsonify(disease)

# Update a Treatment Center
@app.route('/treatment_center', methods=['PUT'])
def update_treatment_center():
    id = request.args.get('id')
    if not id:
        return jsonify({'message': 'No treatment center id provided'}), 400

    treatment_center = TreatmentCenter.query.get(id)
    if not treatment_center:
        return jsonify({'message': 'Treatment Center not found'}), 404

    treatment_center.name = request.json['name']
    treatment_center.attribute_state = request.json['attribute_state']
    treatment_center.attribute_capacity = request.json['attribute_capacity']
    treatment_center.attribute_class = request.json['attribute_class']
    treatment_center.attribute_branch = request.json['attribute_branch']
    treatment_center.website = request.json['website']
    treatment_center.city = request.json['city']
    treatment_center.address = request.json['address']
    treatment_center.image = request.json['image']
    treatment_center.contact = request.json['contact']
    treatment_center.description = request.json['description']

    treatment_center.coordinates.lat = request.json['coordinates']['lat']
    treatment_center.coordinates.lng = request.json['coordinates']['lng']

    db.session.commit()

    # NOTE: no update to connected_treatments or connected_diseases

    return treatment_center_schema.jsonify(treatment_center)

# Update a Treatment
@app.route('/treatment', methods=['PUT'])
def update_treatment():
    id = request.args.get('id')
    if not id:
        return jsonify({'message': 'No treatment id provided'}), 400

    treatment = Treatment.query.get(id)
    if not treatment:
        return jsonify({'message': 'Treatment not found'}), 404

    treatment.name = request.json['name']
    treatment.attribute_class = request.json['attribute_class']
    treatment.attribute_type = request.json['attribute_type']
    treatment.attribute_cost = request.json['attribute_cost']
    treatment.attribute_availability = request.json['attribute_availability']
    treatment.attribute_success_rate = request.json['attribute_success_rate']
    treatment.image = request.json['image']
    treatment.video = request.json['video']
    treatment.overview = request.json['overview']

    db.session.commit()

    # NOTE: no update to side_effects, connected_diseases, or connected_centers

    return treatment_schema.jsonify(treatment)

# Delete Disease
@app.route('/disease', methods=['DELETE'])
def delete_disease():
    id = request.args.get('id')
    if id:
        disease = Disease.query.get(id)

        if not disease:
            return jsonify({'message': 'Disease not found'}), 404

        db.session.delete(disease)
        db.session.commit()

        return disease_schema.jsonify(disease)
    all_diseases = Disease.query.all()
    result = diseases_schema.dump(all_diseases)
    for disease in all_diseases:
        db.session.delete(disease)
    db.session.commit()
    return jsonify(result)


# Delete Treatment Center
@app.route('/treatment_center', methods=['DELETE'])
def delete_treatment_center():
    id = request.args.get('id')
    if id:
        treatment_center = TreatmentCenter.query.get(id)

        if not treatment_center:
            return jsonify({'message': 'Treatment Center not found'}), 404

        db.session.delete(treatment_center)
        db.session.commit()

        return treatment_center_schema.jsonify(treatment_center)
    all_treatment_centers = TreatmentCenter.query.all()
    result = treatment_centers_schema.dump(all_treatment_centers)
    for center in all_treatment_centers:
        db.session.delete(center)
    db.session.commit()
    return jsonify(result)

# Delete Treatment
@app.route('/treatment', methods=['DELETE'])
def delete_treatment():
    id = request.args.get('id')
    if id:
        treatment = Treatment.query.get(id)

        if not treatment:
            return jsonify({'message': 'Treatment not found'}), 404

        db.session.delete(treatment)
        db.session.commit()

        return treatment_schema.jsonify(treatment)
    all_treatments = Treatment.query.all()
    result = treatments_schema.dump(all_treatments)
    for treatment in all_treatments:
        db.session.delete(treatment)
    db.session.commit()
    return jsonify(result)


@app.route("/")
def home():
    return "Welcome to the backend!"


@app.route("/whois/<name>")
def whois(name):
    return "Hello, " + name + ", that is your name!"


# Run Server
if __name__ == "__main__":
    debug = True
    if os.environ.get('ENV') == 'development':
        debug = True
    elif os.environ.get('ENV') == 'production':
        debug = False
    app.run(port=5000, debug=debug, host='0.0.0.0')
