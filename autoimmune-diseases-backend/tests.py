import unittest
from unittest.mock import patch, Mock, MagicMock
from app import app, Disease, Treatment, TreatmentCenter


class TestApp(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()
        self.Disease = Disease
        self.Treatment = Treatment
        self.TreatmentCenter = TreatmentCenter
    """
    class DiseaseSchema(ma.Schema):
        symptoms = ma.Pluck('SymptomSchema', 'symptom', many=True)
        connected_treatments = ma.Pluck('TreatmentSchema', 'id', many=True)
        connected_centers = ma.Pluck('TreatmentCenterSchema', 'id', many=True)
        class Meta:
            fields = ('id', 'name', 'attribute_type', 'attribute_rarity',
                    'attribute_year_discovered', 'attribute_mortality_rate', 'attribute_age_of_onset', 'leading_cause', 'overview', 'symptoms', 'video', 'image', 'connected_treatments', 'connected_centers')
    """
    # disease tests
    def test_get_diseases(self):
        with app.app_context():
            mock_disease1 = {
                'id': 1,
                'name': "Disease1",
                'attribute_type': "Type1",
                'attribute_rarity': "Rare",
                'attribute_year_discovered': 2000,
                'attribute_mortality_rate': 0.1,
                'attribute_age_of_onset': 30,
                'leading_cause': "Cause1",
                'overview': "Overview1",
                'symptoms': [],
                'video': "Video1",
                'image': "Image1",
                'connected_treatments': [],
                'connected_centers': []
            }
            mock_disease2 = {
                'id': 2,
                'name': "Disease2",
                'attribute_type': "Type2",
                'attribute_rarity': "Common",
                'attribute_year_discovered': 1990,
                'attribute_mortality_rate': 0.2,
                'attribute_age_of_onset': 40,
                'leading_cause': "Cause2",
                'overview': "Overview2",
                'symptoms': [],
                'video': "Video2",
                'image': "Image2",
                'connected_treatments': [],
                'connected_centers': []
            }
            Disease.query = MagicMock()
            Disease.query.all.return_value = [mock_disease1, mock_disease2]
            response = self.client.get('/disease')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data), 2)
            self.assertEqual(json_data[0]['name'], "Disease1")
    
    def test_get_disease(self):
        with app.app_context():
            mock_disease = {
                'id': 1,
                'name': "Disease1",
                'attribute_type': "Type1",
                'attribute_rarity': "Rare",
                'attribute_year_discovered': 2000,
                'attribute_mortality_rate': 0.1,
                'attribute_age_of_onset': 30,
                'leading_cause': "Cause1",
                'overview': "Overview1",
                'symptoms': [],
                'video': "Video1",
                'image': "Image1",
                'connected_treatments': [],
                'connected_centers': []
            }
            Disease.query = MagicMock()
            Disease.query.get.return_value = mock_disease
            response = self.client.get('/disease?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Disease1")

    def test_get_disease_not_found(self):
        with app.app_context():
            Disease.query = MagicMock()
            Disease.query.get.return_value = None
            response = self.client.get('/disease?id=1')
            self.assertEqual(response.status_code, 404)

    

    """
    class TreatmentSchema(ma.Schema):
        side_effects = ma.Pluck('SideEffectSchema', 'side_effect', many=True)
        connected_centers = ma.Pluck('TreatmentCenterSchema', 'id', many=True)
        connected_diseases = ma.Pluck('DiseaseSchema', 'id', many=True)
        class Meta:
            fields = ('id', 'name', 'attribute_class', 'attribute_type',
                    'attribute_cost', 'attribute_availability', 'attribute_success_rate', 'image', 'video', 'overview', 'side_effects', 'connected_diseases', 'connected_centers')
    """
    # treatment tests
    def test_get_treatments(self):
        with app.app_context():
            mock_treatment1 = {
                'id': 1,
                'name': "Treatment1",
                'attribute_class': "Class1",
                'attribute_type': "Type1",
                'attribute_cost': 100,
                'attribute_availability': "Available",
                'attribute_success_rate': 0.1,
                'image': "Image1",
                'video': "Video1",
                'overview': "Overview1",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            mock_treatment2 = {
                'id': 2,
                'name': "Treatment2",
                'attribute_class': "Class2",
                'attribute_type': "Type2",
                'attribute_cost': 200,
                'attribute_availability': "Unavailable",
                'attribute_success_rate': 0.2,
                'image': "Image2",
                'video': "Video2",
                'overview': "Overview2",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            Treatment.query = MagicMock()
            Treatment.query.all.return_value = [mock_treatment1, mock_treatment2]
            response = self.client.get('/treatment')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data), 2)
            self.assertEqual(json_data[0]['name'], "Treatment1")
            self.assertEqual(json_data[1]['name'], "Treatment2")

    def test_get_treatment(self):
        with app.app_context():
            mock_treatment = {
                'id': 1,
                'name': "Treatment1",
                'attribute_class': "Class1",
                'attribute_type': "Type1",
                'attribute_cost': 100,
                'attribute_availability': "Available",
                'attribute_success_rate': 0.1,
                'image': "Image1",
                'video': "Video1",
                'overview': "Overview1",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            Treatment.query = MagicMock()
            Treatment.query.get.return_value = mock_treatment
            response = self.client.get('/treatment?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Treatment1")
    
    def test_get_treatment_not_found(self):
        with app.app_context():
            Treatment.query = MagicMock()
            Treatment.query.get.return_value = None
            response = self.client.get('/treatment?id=1')
            self.assertEqual(response.status_code, 404)
    
    """
    class TreatmentCenterSchema(ma.Schema):
        coordinates = ma.Nested('CoordinatesSchema')
        connected_treatments = ma.Pluck('TreatmentSchema', 'id', many=True)
        connected_diseases = ma.Pluck('DiseaseSchema', 'id', many=True)
        class Meta:
            fields = ('id', 'name', 'attribute_state', 'attribute_capacity',
                    'attribute_class', 'attribute_branch', 'website', 'city', 'address', 'image', 'coordinates', 'contact', 'description', 'connected_diseases', 'connected_treatments')
    """
    # treatment center tests
    def test_get_treatment_centers(self):
        with app.app_context():
            mock_center1 = {
                'id': 1,
                'name': "Center1",
                'attribute_state': "State1",
                'attribute_capacity': 100,
                'attribute_class': "Class1",
                'attribute_branch': "Branch1",
                'website': "Website1",
                'city': "City1",
                'address': "Address1",
                'image': "Image1",
                'coordinates': {},
                'contact': "Contact1",
                'description': "Description1",
                'connected_diseases': [],
                'connected_treatments': []
            }
            mock_center2 = {
                'id': 2,
                'name': "Center2",
                'attribute_state': "State2",
                'attribute_capacity': 200,
                'attribute_class': "Class2",
                'attribute_branch': "Branch2",
                'website': "Website2",
                'city': "City2",
                'address': "Address2",
                'image': "Image2",
                'coordinates': {},
                'contact': "Contact2",
                'description': "Description2",
                'connected_diseases': [],
                'connected_treatments': []
            }
            TreatmentCenter.query = MagicMock()
            TreatmentCenter.query.all.return_value = [mock_center1, mock_center2]
            response = self.client.get('/treatment_center')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data), 2)
            self.assertEqual(json_data[0]['name'], "Center1")
            self.assertEqual(json_data[1]['name'], "Center2")

    def test_get_treatment_center(self):
        with app.app_context():
            mock_center = {
                'id': 1,
                'name': "Center1",
                'attribute_state': "State1",
                'attribute_capacity': 100,
                'attribute_class': "Class1",
                'attribute_branch': "Branch1",
                'website': "Website1",
                'city': "City1",
                'address': "Address1",
                'image': "Image1",
                'coordinates': {},
                'contact': "Contact1",
                'description': "Description1",
                'connected_diseases': [],
                'connected_treatments': []
            }
            TreatmentCenter.query = MagicMock()
            TreatmentCenter.query.get.return_value = mock_center
            response = self.client.get('/treatment_center?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Center1")

    def test_get_treatment_center_not_found(self):
        with app.app_context():
            TreatmentCenter.query = MagicMock()
            TreatmentCenter.query.get.return_value = None
            response = self.client.get('/treatment_center?id=1')
            self.assertEqual(response.status_code, 404)

            
    def test_v2_get_disease(self):
        with app.app_context():
            mock_disease = {
                'id': 1,
                'name': "Disease1",
                'attribute_type': "Type1",
                'attribute_rarity': "Rare",
                'attribute_year_discovered': 2000,
                'attribute_mortality_rate': 0.1,
                'attribute_age_of_onset': 30,
                'leading_cause': "Cause1",
                'overview': "Overview1",
                'symptoms': [],
                'video': "Video1",
                'image': "Image1",
                'connected_treatments': [],
                'connected_centers': []
            }
            Disease.query = MagicMock()
            Disease.query.get.return_value = mock_disease
            response = self.client.get('/v2/disease?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Disease1")

    def test_v2_get_diseases(self):
        """
        example response:
        return jsonify(
        {
            "results": result,
            "page": page,
            "per_page": per_page,
            "total_diseases": total_diseases,
        }
        """
        with app.app_context():
            mock_disease1 = {
                'id': 1,
                'name': "Disease1",
                'attribute_type': "Type1",
                'attribute_rarity': "Rare",
                'attribute_year_discovered': 2000,
                'attribute_mortality_rate': 0.1,
                'attribute_age_of_onset': 30,
                'leading_cause': "Cause1",
                'overview': "Overview1",
                'symptoms': [],
                'video': "Video1",
                'image': "Image1",
                'connected_treatments': [],
                'connected_centers': []
            }
            mock_disease2 = {
                'id': 2,
                'name': "Disease2",
                'attribute_type': "Type2",
                'attribute_rarity': "Common",
                'attribute_year_discovered': 1990,
                'attribute_mortality_rate': 0.2,
                'attribute_age_of_onset': 40,
                'leading_cause': "Cause2",
                'overview': "Overview2",
                'symptoms': [],
                'video': "Video2",
                'image': "Image2",
                'connected_treatments': [],
                'connected_centers': []
            }
            Disease.query = MagicMock()
            Disease.query.all.return_value = [mock_disease1, mock_disease2]
            response = self.client.get('/v2/disease?search=Doesnt_match')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data['results']), 0)
            self.assertEqual(json_data['total_diseases'], 0)
            self.assertEqual(json_data['page'], 1)
            self.assertEqual(json_data['per_page'], 20)


    def test_v2_get_treatments(self):
        with app.app_context():
            mock_treatment1 = {
                'id': 1,
                'name': "Treatment1",
                'attribute_class': "Class1",
                'attribute_type': "Type1",
                'attribute_cost': 100,
                'attribute_availability': "Available",
                'attribute_success_rate': 0.1,
                'image': "Image1",
                'video': "Video1",
                'overview': "Overview1",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            mock_treatment2 = {
                'id': 2,
                'name': "Treatment2",
                'attribute_class': "Class2",
                'attribute_type': "Type2",
                'attribute_cost': 200,
                'attribute_availability': "Unavailable",
                'attribute_success_rate': 0.2,
                'image': "Image2",
                'video': "Video2",
                'overview': "Overview2",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            Treatment.query = MagicMock()
            Treatment.query.all.return_value = [mock_treatment1, mock_treatment2]
            response = self.client.get('/v2/treatment?search=Doesnt_match')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data['results']), 0)
            self.assertEqual(json_data['total_treatments'], 0)
            self.assertEqual(json_data['page'], 1)
            self.assertEqual(json_data['per_page'], 20)


    def test_v2_get_treatment(self):
        with app.app_context():
            mock_treatment = {
                'id': 1,
                'name': "Treatment1",
                'attribute_class': "Class1",
                'attribute_type': "Type1",
                'attribute_cost': 100,
                'attribute_availability': "Available",
                'attribute_success_rate': 0.1,
                'image': "Image1",
                'video': "Video1",
                'overview': "Overview1",
                'side_effects': [],
                'connected_diseases': [],
                'connected_centers': []
            }
            Treatment.query = MagicMock()
            Treatment.query.get.return_value = mock_treatment
            response = self.client.get('/v2/treatment?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Treatment1")
    

    def test_v2_get_treatment_centers(self):
        with app.app_context():
            mock_center1 = {
                'id': 1,
                'name': "Center1",
                'attribute_state': "State1",
                'attribute_capacity': 100,
                'attribute_class': "Class1",
                'attribute_branch': "Branch1",
                'website': "Website1",
                'city': "City1",
                'address': "Address1",
                'image': "Image1",
                'coordinates': {},
                'contact': "Contact1",
                'description': "Description1",
                'connected_diseases': [],
                'connected_treatments': []
            }
            mock_center2 = {
                'id': 2,
                'name': "Center2",
                'attribute_state': "State2",
                'attribute_capacity': 200,
                'attribute_class': "Class2",
                'attribute_branch': "Branch2",
                'website': "Website2",
                'city': "City2",
                'address': "Address2",
                'image': "Image2",
                'coordinates': {},
                'contact': "Contact2",
                'description': "Description2",
                'connected_diseases': [],
                'connected_treatments': []
            }
            TreatmentCenter.query = MagicMock()
            TreatmentCenter.query.all.return_value = [mock_center1, mock_center2]
            response = self.client.get('/v2/treatment_center?search=Doesnt_match')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(len(json_data['results']), 0)
            self.assertEqual(json_data['total_treatment_centers'], 0)
            self.assertEqual(json_data['page'], 1)
            self.assertEqual(json_data['per_page'], 20)


    def test_v2_get_treatment_center(self):
        with app.app_context():
            mock_center = {
                'id': 1,
                'name': "Center1",
                'attribute_state': "State1",
                'attribute_capacity': 100,
                'attribute_class': "Class1",
                'attribute_branch': "Branch1",
                'website': "Website1",
                'city': "City1",
                'address': "Address1",
                'image': "Image1",
                'coordinates': {},
                'contact': "Contact1",
                'description': "Description1",
                'connected_diseases': [],
                'connected_treatments': []
            }
            TreatmentCenter.query = MagicMock()
            TreatmentCenter.query.get.return_value = mock_center
            response = self.client.get('/v2/treatment_center?id=1')
            self.assertEqual(response.status_code, 200)
            json_data = response.get_json()
            self.assertEqual(json_data['name'], "Center1")
    



if __name__ == "__main__":
    unittest.main()
