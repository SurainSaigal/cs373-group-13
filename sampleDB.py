# create_engine: creates engine to connect to (MySQL)
from sqlalchemy import create_engine
# ForeignKey: relationship of database being defined
from sqlalchemy import ForeignKey
# column & data types for column
from sqlalchemy import Column, Integer, Float, String, CHAR, Date
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import sessionmaker
# from sqlalchemy.orm import relationship


# connect script to MySQL database
import mysql.connector

# connection
mydb = mysql.connector.connect(
    host="database-1.cducy6y60ir1.us-east-2.rds.amazonaws.com",
    user="admin",
    password="AutoimmuneTeam2024!",
    database="Autoimmune_Prod"
)

# cursor object to execute SQL queriess
mycursor = mydb.cursor()



Base = declarative_base()

class Diseases(Base):
    __tablename__ = 'diseases'
    
    id = Column('id', Integer, primary_key=True)
    disease_name = Column('disease_name', String)
    type = Column('type', String)
    rarity = Column('rarity', String)
    yr_discovered = Column('yr_discovered', String)
    # change to String for '%' sign??
    mortality = Column('mortality', String)
    age_of_onset = Column('age_of_onset', String)
    leading_cause = Column('leading_cause', String)


    def __init__(self, id, disease_name, type, rarity, yr_discovered, mortality, age_of_onset, leading_cause):
        self.id = id
        self.disease_name = disease_name
        self.type = type
        self.rarity = rarity
        self.yr_discovered = yr_discovered
        self.mortality = mortality
        self.age_of_onset = age_of_onset
        self.leading_cause = leading_cause
    
    def __repr__(self):
        return f"({self.id}) {self.disease_name} ({self.type},{self.rarity},{self.yr_discovered},{self.mortality},{self.age_of_onset},{self.leading_cause})"

    
engine = create_engine("sqlite:///mydb.db", echo=True)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

disease = Diseases(1, "Lupus", "Organ Specific", "Common", "1904", "2.65%", "15-44", "unknown")
session.add(disease)
session.commit()

d1 = Diseases(2, "t1d", "Organ Specific", "Common", "1904", "2.65%", "15-44", "unknown")
d2 = Diseases(3, "arthritis", "Organ Specific", "Common", "1904", "2.65%", "15-44", "unknown")
d3 = Diseases(44, "IBD", "Organ Specific", "Common", "1904", "2.65%", "15-44", "unknown")

session.add(d1)
session.add(d2)
session.add(d3)
session.commit()

results = session.query(Diseases).all()
print(results)


# execute SQL query
mycursor.execute("SELECT * FROM your_table")
result = mycursor.fetchall()
for row in result:
    print(row)

# close connection
mydb.close()
