# host the Website to check locally
host:
	$(MAKE) -C autoimmune-diseases-frontend host


build:
	$(MAKE) -C autoimmune-diseases-frontend build

test-frontend-jest:
	$(MAKE) -C autoimmune-diseases-frontend jest

test-frontend-selenium:
	$(MAKE) -C autoimmune-diseases-frontend selenium


test-backend:
	$(MAKE) -C autoimmune-diseases-backend test

#pull from repo
pull:
	git pull
	git status

#push to repo
push:
	git add .
	git commit -m "Default commit message"
	git push
	git status

